{
	writeShellApplication,
	nodePackages,
}:
writeShellApplication {
	name = "bmod-dev";
	runtimeInputs = with nodePackages; [nodejs npm];
	text = ''
		export STATIC=_site
		export BMOD_SERVE_STATIC=1
		#(trap 'kill 0' SIGINT; npm run api:serve & npm run eleventy:watch)
		npm run api:dev
	'';
}
