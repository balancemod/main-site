{
	buildNpmPackage,
	stage ? "dev",
	extraBuildOptions ? {},
	...
}:
buildNpmPackage ({
		name = "bmod";
		src = ./..;
		outputs = ["out" "static" "posts"];
		npmDepsHash = builtins.readFile ../package-lock.sha256;
		npmBuildScript = "static:build";
		NODE_OPTIONS = "--max-old-space-size=2048";
		NODE_ENV =
			if stage == "dev"
			then "development"
			else "production";
		ELEVENTY_ENV =
			if stage == "dev"
			then "development"
			else "production";
		BMOD_SITE_STAGE = stage;
		#postBuild = ''
		#	cp -r _site $eleventy
		#'';
	}
	// extraBuildOptions)
