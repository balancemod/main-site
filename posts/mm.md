---
redirect_to:
  - '/mm'
redirect_from:
  - '/categories/mm'
  - '/posts/updates/mm'
---