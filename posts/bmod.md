---
redirect_to:
  - '/bmod'
redirect_from:
  - '/categories/bmod'
  - '/posts/updates/bmod'
---