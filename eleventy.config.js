import esbuild from "esbuild";
import fs from "node:fs/promises";
import matter from "gray-matter";
import { JSDOM } from "jsdom";
import TurndownService from "turndown";
import { sassPlugin } from "esbuild-sass-plugin";
import path from "node:path";

const output = process.env.static || "_site";

export default async function (eleventyConfig) {
	eleventyConfig.setLiquidOptions({
		dynamicPartials: false, // disable quoted includes
		strictVariables: false,
		//strictFilters: false,
		//jekyllInclude: true,
		//root: [
		//	'_includes',
		//	'.'
		//]
	});

	eleventyConfig.addWatchTarget("assets");
	eleventyConfig.addLiquidFilter("is_array", arr => Array.isArray(arr));
	eleventyConfig.addLiquidFilter(
		"is_object",
		obj => obj && typeof obj === "object",
	);
	eleventyConfig.addLiquidFilter(
		"where_exp",
		function where_exp(arr, arg, exp) {
			if (!Array.isArray(arr)) {
				return arr;
			}
			return arr.filter(item => {
				const liquid = `{% if ${exp} %}1{% endif %}`;
				const context = Object.assign({}, ...this.context.scopes, {
					[arg]: item,
				});
				const output = this.liquid.parseAndRenderSync(liquid, {
					...this.context.environments,
					...context,
				});
				return !!output;
			});
		},
	);
	eleventyConfig.addLiquidFilter("sort", function sortArrayByKey(arr, key) {
		if (!Array.isArray(arr)) {
			return arr;
		}
		const useKey = key && typeof key === "string";
		const sorted = arr.toSorted((a, b) => {
			//if (a === b) {
			//	return 0;
			//}
			a = useKey ? a[key] : a;
			b = useKey ? b[key] : b;
			if (key === "date") {
				a = new Date(a).toISOString();
				b = new Date(b).toISOString();
			}
			let sorted = [a, b].sort();
			if (typeof a === "number" && typeof b === "number") {
				return a - b;
			}
			return sorted[0] === b ? 1 : -1;
		});

		return sorted;
	});

	eleventyConfig.addLiquidFilter("query", function query(content, tag) {
		if (!content || !tag) {
			return [];
		}

		const elements = new JSDOM(content).window.document.querySelector(tag);
		if (!Array.isArray(elements)) {
			return elements;
		}

		return Array.from(elements, element => {
			const attributes = {};
			for (let i = 0; i < element.attributes.length; i++) {
				const attr = element.attributes[i];
				attributes[attr.name] = attr.value;
			}
			return attributes;
		});
	});

	const turndownService = new TurndownService();

	// Add the filter to Eleventy
	eleventyConfig.addFilter("htmlToMd", html =>
		turndownService.turndown(html),
	);
	eleventyConfig.addFilter("getRaw", page => page.rawInput);

	eleventyConfig.addLiquidFilter("random", arr => {
		if (!Array.isArray(arr)) {
			return null;
		}
		return arr[Math.floor(Math.random() * arr.length)];
	});

	eleventyConfig.addLiquidFilter("toISO8601", date => {
		if (!(date instanceof Date)) {
			date = new Date(date);
		}
		return date.toISOString();
	});

	//eleventyConfig.addPassthroughCopy("assets");
	eleventyConfig.addPassthroughCopy("assets/downloads");
	eleventyConfig.addPassthroughCopy("*.zip");
	eleventyConfig.addPassthroughCopy("assets/fonts");
	eleventyConfig.addPassthroughCopy("assets/images");
	eleventyConfig.addPassthroughCopy("assets/xsl");
	eleventyConfig.addPassthroughCopy("assets/testimonials");
	eleventyConfig.addPassthroughCopy("assets/styles/*.css");
	eleventyConfig.addPassthroughCopy("assets/manifest.json");

	eleventyConfig.ignores.add("_dynamic");

	eleventyConfig.addDataExtension(
		"yml, yaml",
		//(contents) => parseFM(`---\n${contents}\n---`, file),
		contents => matter(`---\n${contents}\n---`).data,
	);
	const siteStage =
		process.env.BMOD_SITE_STAGE ||
		(process.env.NODE_ENV === "production" ? "prod" : "dev");

	eleventyConfig.addGlobalData("site", {
		stage: siteStage,
		url:
			{
				dev: "http://localhost:3000",
				prod: "https://bmod.tf",
				11: "https://11.beta.bmod.tf",
			}[siteStage] || `https://${siteStage}.bmod.tf`,
			testimonials: await  (async (testimonialsPath)=> {
				const dir = path.join(import.meta.dirname,testimonialsPath);
					const files = (await fs.readdir(dir)).filter(file=>{
						return path.extname(file).toLowerCase() === ".png";
					})
					return files
			})("assets/testimonials")
	});

	//eleventyConfig.addGlobalData("layout","default")
	eleventyConfig.addGlobalData("container", true);
	eleventyConfig.addGlobalData("background-head", false);
	eleventyConfig.addGlobalData("margin", true);
	//eleventyConfig.addGlobalData("date","Last Modified")

	for (const collection of ["blog", "updates", "bmod", "mm"]) {
		eleventyConfig.addCollection(collection, collectionApi =>
			collectionApi
				.getAllSorted()
				.filter(item => {
					// Side-step tags and do your own filtering
					const cat = item.data.categories || item.data.category;
					return Array.isArray(cat)
						? cat.includes(collection)
						: cat === collection;
				})
				.reverse(),
		);
	}

	// js and scss
	eleventyConfig.on("eleventy.before", async () => {
		await esbuild.build({
			entryPoints: ["assets/scripts/*.js", "assets/styles/*.scss"],
			outdir: `${output}/assets`,
			minify: process.env.ELEVENTY_ENV === "production",
			sourcemap: process.env.ELEVENTY_ENV !== "production",
			treeShaking: process.env.ELEVENTY_ENV === "production",
			plugins: [sassPlugin()],
			loader: {
				".jpg": "file",
				".png": "file",
				".ttf": "file",
			},
			assetNames: "[dir]/[name]",
			chunkNames: "scripts/chunks/[name]-[hash]",
			entryNames: "[dir]/[name]",
			splitting: true,
			format: "esm",
			bundle: true,
			alias: {
				"@data": "./data",
			},
		});
	});

	async function articleConciser({
		page,
		data: {
			site,
			staff,
			author: authorName,
			title,
			tags,
			category,
			categories,
		},
		template,
	}) {
		let content = "";
		const author =
			staff.members.find(member => member.title === authorName) ||
			authorName;

		try {
			if (template.read) {
				const read = await template.read();
				if (read.content) {
					content = read.content;
				}
			}
		} catch (error) {
			console.warn(`failed to read template for ${title}`, error);
		}

		return {
			title,
			url: site.url + page.url,
			date: page.date,
			summary: content,
			collections: tags,
			category: categories || category,
			author: author
				? {
						name: author.title,
						url: author.url,
						iconURL: author.icon,
					}
				: undefined,
		};
	}

	// webhook
	eleventyConfig.addCollection("dfasf", async collectionApi => {
		//await webhook(webhookConfig, collectionApi.getAllSorted())
		if ("posts" in process.env) {
			const posts = await Promise.all(
				collectionApi.getAllSorted().map(articleConciser),
			);
			await fs.writeFile(
				process.env.posts,
				JSON.stringify(posts),
				"utf8",
			);
		}
		return [];
	});

	return {
		dir: {
			layouts: "_layouts",
			output,
		},
		passthroughFileCopy: true,
	};
}
