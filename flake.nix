{
	description = "Bmod.tf website";
	inputs = {
		nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
		tumblepkgs = {
			url = "git+https://git.disroot.org/tumble/tumblepkgs";
			inputs.nixpkgs.follows = "nixpkgs";
		};
	};
	outputs = {
		nixpkgs,
		...
	}: let
		system = "x86_64-linux";
		pkgs = import nixpkgs {inherit system;};
		bmod = pkgs.callPackage ./pkgs/bmod.nix {stage = "prod";};
	in {
		defaultPackage.${system} = bmod;
		packages.${system} = {
			default = bmod;
			inherit bmod;
			dev = pkgs.callPackage ./pkgs/bmod-dev.nix {};
			test = bmod.override {stage = "beta";};
		};

		devShell.${system} = with pkgs; mkShell {packages = [npm];};
	};
}
