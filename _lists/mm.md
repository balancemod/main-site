---
title: Manned Machines
redirect_from:
- /archive/
show_categories: true
list:
  collection: mm
  meta: true
  content: true
  date: true
  #excerpt: true
---