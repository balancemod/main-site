---
title: Blog
excerpt: "List of all posts on the website"
redirect_from:
- /archive/
- /posts/
show_categories: true
list:
  collection: blog
  meta: true
  content: true
  date: true
  #excerpt: true
---
