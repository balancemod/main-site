---
title: Posts
excerpt: "List of all posts on the website"
redirect_from:
- /archive/
show_categories: true
list:
  collection: posts 
  meta: true
  content: true
  date: true
  #excerpt: true
---