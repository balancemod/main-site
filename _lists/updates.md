---
title: Updates
excerpt: "List of all posts on the website"
redirect_from:
- /archive/
show_categories: true
list:
  collection: updates
  meta: true
  content: true
  date: true
  #excerpt: true
---