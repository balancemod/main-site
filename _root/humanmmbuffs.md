---
title: Human Buffs in Manned Machines
---

<h3>Multi-Class</h3>

+ Shotgun: +20% reload and firing speed and 1 point of the Bullet Penetration upgrade
+ Reserve Shooter: +40% deploy speed, mini-crits become crits, and crits for 2x damage vs blast-jumping robots
+ Half-Zatoichi: +15 health on hit
+ All other Damaging Taunts: 2.5x damage dealt by taunt

<h3>Scout</h3>

+ Passive: Always respawn around 4 seconds after death, +20% faster reload speed
+ All Scatterguns & Pistols: 1 point of the Bullet Penetration upgrade
+ Pistol: Gain a speed boost on hit for 2 seconds
+ Cleaver: Bleed lasts for 10 seconds
+ Stock melee & Reskins: Provides all weapons with +50% max ammo and +25% reload speed
+ Atomizer: +3 bonus jumps instead of +1 bonus jump, mini-crits become crits
+ Candy Cane: +33% more health from healthpacks and +4 passive health regen
+ Sandman Baseball: Target is disoriented
+ Wrap Assassin Ornament: Reduce robot heal rate on hit by 50% for 5 seconds
+ Sun-on-a-stick: Gain the ability to use the Fireball spell, has a 30 second cooldown

<h3>Soldier</h3>

+ All Rocket Launchers: +20% reload speed
+ Air Strike: Gain 1 additional clip for every 250 damage dealt to robots
+ Black Box: Up to +30 health on hit
+ Beggars Bazooka: +3 clip size
+ Liberty Launcher: Provides all banners +200% longer buff duration
+ Rocket Launcher: +50% larger explosion radius
+ Cow Mangler & The Righteous Bison: Reduce robot heal rate on hit by 50% for 1 second. Also reduce shield meter by 5% vs robot medics with an active shield
+ Manntreads: Target is disoriented, +100% stomp damage bonus
+ Gunboats: No fall damage on wearer
+ Market Gardener: +50% damage while blast-jumping
+ Kamikaze Grenade Taunt: 3.5x damage dealt by taunt

<h3>Pyro</h3>

+ Passive: +50% fire damage resistance
+ Third Degree: Crits players who are being healed and hits all players connected to the same heal source
+ Thermal Thruster: Deals no knockback to robots when landing
+ Axtinguisher: Battalion's Backup buff is gained along with the speed boost, both effects last for 5 seconds
+ Powerjack: +50 health on hit, health gained can overheal
+ Hadouken Taunt: 3x damage dealt by taunt

<h3>Demoman</h3>

+ Passive: When dealing damage, gain a stackable damage bonus that decays over time. Bonus damage is capped at +40%
+ All Projectile Weapons: +25% reload speed
+ Quickiebomb Launcher: Up to +65% damage based on charge instead of +35%
+ Stock melee & Reskins: +20 max health
+ Caber: +20% damage bonus on the hit that causes the explosion. Crits become mini-crits
+ Demoknight: +25% faster melee swing speed, +25% bullet, melee and crit damage resistance
+ Eyelander: Gains a head every hit vs robots
+ The Claidheamh Mòr: Charge time increased by 2 seconds instead of 0.5
+ Shield Impact: Reduce robot heal rate by 50% and stun robot for 0.5 seconds upon impacting a "Tank" type robot

<h3>Heavy</h3>

+ All Miniguns: -20% damage
+ Tomsislav: Mini-crits targets when fired at their back from close range. +60% faster rev up speed
+ Natascha: Gain a speed boost on hit for 3 seconds. +20% faster movement speed while spun up
+ Stock melee & Reskins: +50% max ammo on all weapons
+ Warrior's Spirit: +50 health on hit, health gained can overheal
+ KGB: Gain crits for 6 seconds when landing a quick 3 hit combo

<h3>Engineer</h3>

+ Frontier Justice: Gains 1 revenge crit for every 250 damage your sentry does to robots
+ Pomson 6000 & Short Circuit: Reduce robot heal rate on hit by 50% for 1 second. Also reduce shield meter by 5% vs robot medics with an active shield
+ Pistol: +100% clip size
+ Gunslinger: +15% movement speed while active

<h3>Medic</h3>

+ Syringe gun: Dealing damage gives you small amounts of uber. +15% passive uber build rate
+ Blutsauger: Reduce robot heal rate on hit by 35% for 1 second. +20 max health
+ Overdose: Provides your medigun the overheal decay disabled upgrade
+ Crusader's Crossbow: Teammates hit get the Resistance rune effect for 1 second.
+ Quick-Fix: MvM shield ability. Shield has -30% time to build, but -20% duration and deals no damage
+ Vaccinator: +10% to all resistance effects
+ All Melees: "Organs" system from the vanilla Vita-Saw. Maximum uber retained caps at 50%
+ Stock melee & Reskins: Provides your medigun an additional 25% faster build rate
+ Amputator: +70% passive crit resistance
+ Solemn Vow: Provides your medigun with 4 points of the Healing Mastery upgrade. You can see robot medics' uber percentage

<h3>Sniper</h3>

+ Sniper Rifle: 1 point of the Explosive Headshots upgrade
+ Bazaar Bargain: Gain 1 head on headshot, but lose 2 heads on bodyshot
+ Classic: Headshot anytime
+ Huntsman: 1 point of the Projectile Penetration upgrade, 10 seconds of bleed on hit, and +93% max ammo
+ SMG: Gain a speed boost on hit for 2 seconds
+ Razorback: +50% melee damage resistance, will instantly recharge and stun spy robot when triggered
+ Stock melee & Reskins: +15% movement speed while active
+ Shahansah: Fling yourself in the opposite direction on hit
+ Tribalman's Shiv: Bleed lasts for 20 seconds

<h3>Spy</h3>

+ Passive: Use robot teamporters by touching their spawn
+ All Revolvers: Tag robots on hit for 8 seconds and 1 point of the Bullet Penetration upgrade
+ Diamondback: Gain 2 crits on backstab
+ Enforcer: +200% Damage bonus vs sapped buildings
+ Ambassador: No crit damage falloff
+ All knives: Gain the Resistance rune effect on backstab for 1 second. 2x backstab damage if the victim has at least 80% health. +20% backstab damage vs Heavies
+ Knife: Tag robots on hit for 8 seconds
+ Big Earner: Gain a speed boost on backstab for 3 seconds. +15% movement speed while active
+ Spycicle: Reduce all damage dealt by the victim on backstab by 30% for 6 seconds
+ Kunai: +120 health on backstab, health gained can overheal

This page may not always be correct or up to date after an update. The latest code for human buffs can be found on the gamemode's [Github](https://github.com/higps/robogithub).

Any other weapon changes in the gamemode are from Balance Mod, and can be found [here](https://www.bmod.tf/moddedweapons/).

Join the [Discord](https://discord.gg/VMAJgEJ) for pings and announcements for when we play!

Do join the [Steamgroup](https://steamcommunity.com/groups/higps) if you haven't already!
