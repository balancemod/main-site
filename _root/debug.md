---
---
## list of pages

|Title  |Link  |
|---|---|
{% for doc in collections.all -%}
|{{ doc.title }}  | {{ doc.url }}  |
{% endfor %}