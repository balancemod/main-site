---
title: Manned Machines
---

<center>

<img align="center" width="auto" height="auto" src="https://www.bmod.tf/assets/images/mm/MannedMachineBGRender.webp">
</center>


+ __FAQ__

    + What is Manned Machines?
        + It's an assymetric game mode where a team of Giant Robots fights a team of humans.
        + On a full 24 slot servers there'd be 6 robots vs 18 humans.
    + Do I need to download anything to play?
        + No, there are no custom downloads needed to play this mode, but we recommend the assetpack with animation fixes
    + Why should I get the assetpack?
        + Robots have missing animations added
        + Robot cosmetics will not stretch across the map
        + You will hear custom sounds that improves the gameplay
        + Saxtron Hale will no longer be a giant Error sign
    + What are Robot and Boss coins and how do you get them?
        + Robot-Coins are earned whenever a Robot on the Robot team is killed by a Human.
        + Robot-Coins are spent individually (like money in MvM)
        + Boss-Coins are earned whenever a point is captured
        + Boss-Coins are spent per team
    + Is this on Github?
        + Yes, [Here](https://github.com/higps/robogithub)

+ __Buffs for Humans__
    + [Custom Buffs for Humans in this mode](https://www.bmod.tf/humanmmbuffs/)

+ __Robot Animation Fixes__
<a id="asset-pack"></a>
# Asset Pack
{% comment %}
Dear GPS:

please use _data/asset-pack.json for entering the new asset pack url
{% endcomment %}
<a href="{{asset-pack.url}}" class="btn btn-green font-tf2 big">Download animation fixes + assetpack</a>

<center>
<h1>Assetpack vs no Assetpack</h1>

<iframe width="560" height="315" src="https://www.youtube.com/embed/ewztVWeOU5I?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>

__Commands__
+ !join in chat or sm_join in console to volunteer to be a robot
+ !bot in chat ,sm_bot in console or change to change robot while a robot
+ !mminfo or sm_mminfo in console to toggle the chat output of the custom human buffs in chat


 __Map Changes__
+ This gamemode can work on all maps, but is designed for our own versions of Payload and Attack/Defense maps
+ Key Differences:
    + Engineers can build in spawn
    + Most roofs are accesable and build-able



__General Weapon Changes__
+ All weapon changes from Balancemod.tf are present in Manned Machines.
    + [Click here to see the Bmod.TF Changes](/https://www.bmod.tf/moddedweapons)
+ All Engineers can now build Engi-Pads instead of regular Teleporters. Use +attack3 or your +reload command to activate Engi-Pads. This should give you an in-game notification that Engi-Pads are ENABLED or DISABLED . To build one, simply place a Teleporter Entrance or Exit when Engi-Pads are enabled. You can rotate the Teleporter to alternate between the Speed Pad and Jump Pad. For a video demonstration.
+ Many weapons have gotten new stats to better suit the mode. Like explosive headshots on sniper and on kill attributes being made on hit. Experiment and see what you like best. This is displayed every time you spawn or touch a resupply locker. Type !mminfo to toggle the information viewed in chat

<center>
<h1>Humans</h1>
</center>

__Human Changes__

+ Scout
    + Respawns in 6 seconds, you will shortly respawn after the killcam ends
+ Demoman
    + All weapons deal +25% damage, reload +25% faster and demoknight gets resistances and an additional +25% faster weapon swing speed
+ Spy
    + Backstabs deal 250 damage on robots
    + Backstabs deal addtional damage to robots over 80% health

__Human Strategy__

+ Think of it as MvM
+ Robots are slow and cannot contest many areas at once.
+ You can outrange the robots and force them into positions they don't want to be in
+ Coordinating with your teammates is the best strategy for taking down the robots. You can’t reliably beat any robot all by yourself!
+ All robots deal reduced damage to buildings. Engineers can use their buildings to provide safe zones for their teammates and reliable damage against robots. Don’t be afraid to use Level 3 Sentries on Offense! Be wary of the Orange painted ones, they are designed to destroy buildings
+ Most robots are slow and can't chase you. Simply run away, reposition or claim parts of the map they can't contest!


<center><h1>Robots</h1>
</center>

__Robot Types__

+ __Damage__
    + Difficulty: Easy
    + Deals damage consistently
    + Most reliable at dealing with groups of humans
    + Most straightforward to play
    + Lacks specific utility compared to other robot types

+ __Tank__
    + Difficulty: Medium
    + Lots of health, and damage resistance in some cases
    + Deals massive damage in melee range
    + Limited ranged options
    + Some are weak to melee damage
    + Can be avoided by simply running away

+ __Disruptor__
    + Difficulty: Hard
    + Largely gimmick based robots, but all gimmicks have a weakness…
    + Harasses the humans easily from any range
    + Highest mobility of all robots
    + Provides an additional robot-coins on death
+ __Support__
    + Difficulty: Hard
    + Applying Buff Banners
    + Getting specific human player picks
    + Snipers and Spies are in this category
    + Low combat ability by themselves
+ __Healer__
    + Difficulty: Easy
    + Heals
    + Provides various buffs, but never UBER
+ __Bosses__
    + Difficulty: Easy
    + Crit boosted, high health, and extremely dangerous
    + Costs the most amount of robot coins and serves as “final boss” of each round
    + Can't be healed, any damage they take is permanent.

<center><h1>Robot Team Strategy</h1></center>

+ Your most important goal is to play the objective, don't bother killing all humans before doing so.
+ Robot Engineers cannot build Teleporter Entrances, but can build Exits. The Teleporter Exit operates as a Teamporter, which serves as a spawn point for your team after they die. Use this to easily regroup and keep up pressure on the human team, but they have a steep metal cost!
+ Since robots are slow, you can't pick your fights like humans can. Robots need to group up together and focus on the objective as a team. If one robot on your team isn’t contributing, you are 1/6th of a player down on your team, so make sure you are doing your part!
+ Every robot has their uses, and some work better in certain situations than others. Experiment with different robots to find one that suits your team’s playstyle.
Communicate with your team! If you see something is amiss, tell your teammates about it, or switch robots to help out where you see your team struggling.

__Recommended Newbie Robots__

+ Heavy: HiGPS: Damage -> Hitscan
    + Hard to go wrong with this one as you deal OK damage and shoot down projectiles
+ Soldier: Icebear: Damage -> Rockets
<img align="right" width="250" height="auto" src="https://www.bmod.tf/assets/images/mm/bearded2.webp">
    + Basic rocket robot, never a wrong choice
+ Medic: Array Seven: Healer:
    + Kritzkrieg charges fast on robots since they are never overhealed. Kritz are always good on robots since you have it so often.
+ Heavy: Bearded Expense: Tank
    + Easy to use, get a kill to get a combo, has to be respected
+ Demoman: Mortar Mack: Damage -> Grenades
    + Shotgun grenade launcher, will insta delete anything in front of you
+ Pyro: Agro: Damage -> Flames
    + Airblast, and homing scorch shots, also has a maul and offers great protection against spies
+ Engineer: Uncle Dane: Builders
    + Decent Shotgun and Wrench, LVL 3 sentry that is simple to maintain makes him a very easy starter robot. Do note that teamporters is the most important building you have.


<h2>We're looking for map makers!</h2>

+ This mode uses custom modded maps and we're looking for map makers who can either modifiy their old maps to fit this gamemode or create an original map for this mode. Contact HiGPS on the discord or on steam if you're interested!
