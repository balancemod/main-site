---
title: Testimonials
---
{% for testimonial in site.testimonials %}
![](/assets/testimonials/{{testimonial}})
{% else %}
(none)
{% endfor %}