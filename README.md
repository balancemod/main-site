# Balencemod.TF Websiet
> nodejs overhall

## Tools
*  🚀 [Node.js](https://nodejs.org/) - JavaScript runtime built on Chrome's V8 JavaScript engine
* 🚂 [Express.js](https://expressjs.com/) - Fast, unopinionated, minimalist web framework for Node.js
* 📦 [Mime-Types](https://www.npmjs.com/package/mime-types) - A comprehensive library for mime-type mapping
* 🎮 [Valve Server API](https://developer.valvesoftware.com/wiki/Server_queries) - API for querying information about Source engine servers
* 🔌 [Socket.io](https://socket.io/) - Real-time bidirectional event-based communication library

### how we keep hold of [Jekyll](https://jekyllrb.com/)
* 🌊 [Liquid.js](https://liquidjs.com/) - A simple, expressive, and safe template engine for Node.js
* 📘 [Marked](https://marked.js.org/) - A fast markdown parser and compiler for Node.js
### Styling (bmod.css)
* 🌀 [Normalize.css](https://necolas.github.io/normalize.css/) - A modern, HTML5-ready alternative to CSS resets
* 🎨 [Sass](https://sass-lang.com/) - A popular CSS preprocessor scripting language
### Cache
* 🚀 [Superjson](https://www.npmjs.com/package/superjson) - A safer and faster JSON.stringify and JSON.parse


## Setting Up
First install Node version manager and then setup Node
```
nvm use
node i
```
### on nix
if you are on nix first run `nix develop` to enter a shell enviroment where you have node commands.
then run `npm install` to setup the node_modules folder, and then `exit` out of the nix shell.
and then you should beable to run ` nix run .#bmod-dev`

### development
```
npm run dev
```
then nodemod will spam `[nodemon] restarting due to changes...`
halt the command and run again
### production
```
npm start
```

## webserver
### Install
1. clone
```bash
git clone https://gitlab.com/balancemod/main-site.git /tmp/nixconfig
```

2. partition disk
```bash
sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko /tmp/nixconfig/src/disko.nix --arg device /dev/sda
```

3. install nixos
```bash
sudo nixos-install --root /mnt --flake gitlab:balancemod/main-site
```
### Update
```
sudo nixos-rebuild switch --flake gitlab:balancemod/main-site
```

# gitlab runner setup
```
# node - /etc/gitlab-runner/blutsauger-node.env
CI_SERVER_URL=https://gitlab.com
CI_SERVER_TOKEN=TOKEN_HERE


# nix - /etc/gitlab-runner/blutsauger-nix.env
CI_SERVER_URL=https://gitlab.com
CI_SERVER_TOKEN=TOKEN_HERE
```

# Todo
moved to: !224
## permalink notes

- posts: :categories/:year/:month/:day/:slug
- pages: :path/slug
- collection :collection/:path/:slug


# Jekyll
> jekyll overall setup by tumble
## Setting Up
First install Ruby Version Manager or Ruble 2.7.5 and then setup Ruby
```
rvm use
gem install bundle
bundle install
```

## notes
### data
- defaultweapons mercs new wepons - modded weapons
- team - api, credits
- servers - api,servers
- tf2rebalance_attributes_summary.json  - balence

## 11ty =. jekyll
there is no site.
also no include.

## Running
```
bundle exec jekyll server -w --config _config.yml,_config-dev.yml
```


> NO
# not-quite-static
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fzarlo%2Fnot-quite-static.svg?type=shield)](https://app.fossa.io/projects/git%2Bgithub.com%2Fzarlo%2Fnot-quite-static?ref=badge_shield)


for when a static site generator feels like a pain and you dont want to write any php only html and a bit of json and or yaml

## License
[![FOSSA Status](https://app.fossa.io/api/projects/git%2Bgithub.com%2Fzarlo%2Fnot-quite-static.svg?type=large)](https://app.fossa.io/projects/git%2Bgithub.com%2Fzarlo%2Fnot-quite-static?ref=badge_large)
