---
title: "Bonkscout nerf, human Soldier buff"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

<h2>Robots</h2>

+ __Bonk Scout__
    + Changed:
        + Reduced amount of baseballs from 30 to 8
        + Reduced recharge bonus from +90% to +80% faster
            + _Bonk Scout has been very good for a long time, with the insane amount of baseballs offered little counterplay for humans caught in the range, as there was no escape, with less baseballs to stock up on it offers more counterplay while Bonk Scout can still kill if he spends the entire ball clip on a target_

+ __Sentro__
    + Temporarily removed from roster until Medigun AOE healing Uber gets fixed


<h2>Humans</h2>

+ __Soldier__
    + Beggars Bazooka
        + Removed:
            + +15% faster move speed
        + Added:
            + +2 clip size
            + Up to +60% faster firing speed based on remaining health

    + Rocket Launcher
        + Changed:
            + Increased blast radius bonus from +30% to +50%

<h2>Server</h2>

+ __Increased net chan limit (again)__
    + Increased net chan limit, this should prevent players from getting kicked by the system when changing robots sometimes.


Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play