---
title: Hello everybody all over the world!
author: Heavy Is GPS
category: blog
---
We're still working on Balancemod 2.0 and it's going to be great.

Here are some features we are working on

* Game balance to perfection
* Banger trailer video
* Seasons with skill point reset and rankings
* Repeatable contracts that gives unique and rare cosmetic perks on your stat profile, ingame name color or chat.
* Website improvements
* Discord server seeding
* Team balance and matchmaking improvements

Do join the
[discord](https://discord.gg/VMAJgEJ) if you haven't already!!
