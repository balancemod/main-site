---
title: "Sword buffs with Syringe Gun Tweak"
author: Heavy Is GPS
categories: ["updates","bmod"]
---

<h2>Balance</h2>

+ __Syringe Gun__
    + Added:
        + HUD displaying when next charge gets added
    + Changed:
        + Reduced damage needed from 100 to 40 to gain the 15%

+ __The Claidheamh Mòr__
    + Added:
        + +25% increase in self push force
    + Changed:
        + Max Health penalty penalty applies all the time
        + Increased max health penalty from -10 to -15
    + _This is to further enhance the "increased range" for mobility options for demo regardless of item combination. For now this attribute doesn't require the weapon to be active, as the downside is also constantly present._

+ __Persian Persuader__
    + Added:
        + On Hit: +25% faster reloadspeed for 10 seconds
    + _This is to keep with the theme to increased tempo as it does with charge recharge_

Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play

