---
title: "Balancemod: Cow Mangler 5000 buff"
author: Heavy Is GPS
categories: ["updates","bmod"]
---

## Balancemod Balance Changes

__Cow Mangler 5000__

+ Added:
    + +25% charge shot damage bonus
        + _This is to compensate for the 59 damage loss from the afterburn. The damage isn't the same as full afterburn, but somewhere in between making it more powerful_



Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.
