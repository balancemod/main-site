---
title: Pyro Patch for 22 April 2022
author: Heavy Is GPS
categories: ["updates","bmod"]
---
Powerjack:
```
+ Reverted to Vanilla
```

Hot Hand:
```
+ Reduced damage vulnerability from -30% to -20% to match vanilla Powerjack
```

Gas Passer
```
Bugfix: Fixed a bug where Gas Passer could build meter off itself. Chaining gas passer into gas passer is no longer possible.
```

Fire Axe & All-class melee unlock for Pyro
```
- Removed: Fire resistance and mini-crit when lit on fire
+ Added (Experimental): Can now market garden (only works with thermal thruster and reflect rocket jumps)
```

Manmelter
```
+ Changed: Increased health restored with extinguished teammates from 20 to 40
```

Sharpened Volcano Fragment
```
+ Added: On targets on fire hit: Spawns 3 dragon's fury projectiles in the direction the pyro is looking from the victim
- Changed: Reduced burn time from 5 to 3 seconds
- Changed: Reduced explosion damage from 50 to 10
- Bugfix: Explosion can no longer trigger through walls
```

Do join the
[discord](https://discord.gg/VMAJgEJ) if you haven't already!!
