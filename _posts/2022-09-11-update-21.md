---
title: "Balancemod: Vita-Saw"
author: Heavy Is GPS
categories: ["updates","bmod"]
---

## Balancemod Balance Changes


__Vita Saw__
+ Changed:
    + Increased attack speed penalty from -20% to -25% slower
+ Removed:
    + On Hit: Remove mark for death
        + _Being able to remove nearly all buffs is really strong, however mark for death is hard to apply and is a downside on some weapons that could outright be negated entirely with this weapon_


Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play

