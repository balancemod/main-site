--- 
title: "Goodbye Commander Crocket"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

<h2>Humans</h2>

+ __Soldier__
    + Liberty Launcher
        + Bugfix:
            + Fixed an issue where the longer buff duration stayed on the item when switching to another rocket launcher

+ __Debuff Damage bonus__
    + Changed:
        + Reduced the damage increment from +10% to +5% per additional debuff applied. Caps at 15%.


<h2>Robots</h2>

+ __Spies__
    + Changed:
        + Reduced healing from sapping from 50 to 25 health
            + _In certain situations the spies, being as fast as they are would just never ever die with the sustain from sapping buildings_
    
+ __Stabby Stabby__
    + Removed:
        + Revolver
    + Changed:
        + Reduce knife fire rate from +80% to +60% faster
    + Removed:
        + Instant decloak bonuss

+ __Space Laser__
    + Changed:
        + Now fire hitscan instead
        + Hitscan are tracers
    + Removed:
        + Overheat mechanic
    + Added:
        + -70% damage penalty vs buildings

+ __Cloaker__
    + Changed:
        + Cosmetics to be more camo

Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already and let us know what you think about this patch!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play