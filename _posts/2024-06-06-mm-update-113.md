---
title: "We're back! Astro nerf!"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

## Humans

+ __Bleed__
    + Changed:
        + Increased bleed damage by +40%

+ __Demoman__
    + The Loose Cannon
        + Removed:
            + Robot knockback reduction
        + Added:
            + On Hit: Robot take reduced knockback from cannon balls for 3 seconds.
                + _This makes it so you can get full knockback on the robot on the first, but spamming it will result in the robot taking permanent less knockback. This applies to all loose cannons so you can't stack first shots on it to get major knockback_
+ __Soldier__
    + Airstrike
        + Changed:
            + Increased Max Bonus Clip from 8 to 16
        + Fixed:
            + Critical damage only counting the base damage when building up 250 damage for a bonus clip

+ __Scout__
    + Force-A-Nature
        + Added:
            + +40 bonus upward push force when penetrating robots
                + _For some reason this is a bug, where if the robot is shot by themselves it don't apply right, but if the penetration shot hits more than one robot, the pushback is added somehow, it's not a bug, it's a feature!"_

+ __Pyro__
    + Backscratcher:
        + Added:
            + While Active: +50% melee damage resistance

+ __Heavy__
    + Fists of Steel
        + Added:
            + While Active: +50% critical damage resistance

## Robots
+ __Astrobot__
    + Changed:
        + Reduced health from 3000 to 1750
    + Removed:
        + Heal On Kill
    + _we see that robots that have mobility scales way better than the rest with health, so a nerf to this extremely mobile disruptor was needed_

+ __Mortar Mack__
    + Added:
        - -20% damage penalty vs players
            + _This makes pipes deal 80 damage instead of 100_
        + Changed:
            + Increased projectile deviation from +7* to +11*
                + _This combined with the damage reduction makes you able to survive a face blast at slightly further away than before. Also reduces the consistency of the bot at mid to long ranges while still being as lethal up close._

+ __Fat Magic__
    + Changed:
        + Reduce piss swim from 7 to 5 seconds

## Backend
+ Fixed the game crashing constantly
+ Updated the logic of changing knockback for loose cannon and scorch shot to be more dynamic and allow more parameters


+ Join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!
+ Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.
+ [Human Buffs](https://wiki.bmod.tf/Manned_Machines_Human_Buffs)
+ [Robots](https://wiki.bmod.tf/Robot_Overview)

Use the !seed command on our servers to let the discord server know you want to play
