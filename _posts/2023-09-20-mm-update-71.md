--- 
title: "Barricade joins the roster"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

<h2>Humans</h2>

+ __Soldier__
    + Air Strike:
        + Changed:
            + Increased clip size increase on kill from 4 to 8
    + Liberty Launcher:
        + Added:
            + Gunboats: -90% rocket jump damage
            + Mantreads: +30% movespeed bonus
                + _Now Liberty Launcher gives bonuses to all soldiers unlocks_


+ __Shotguns__
    + Added:
        + +25% bonus damage vs jumping robots

+ __Reserve Shooter__
    + Changed:
        + Reduced the damage bonus vs jumping robots from +100% to +75%
            + _The thought behind the shotguns dealing more damage to robots when they are jumping is to make robots pick a ground game or a jumping game. Where snipers are good vs non-jumping robots and shotguns are good vs jumping robots, meaning the robots can choose which option to pick depending on what the humans do. If there are multiple shotgun users it will be better for them to not jump even if there are a few snipers._


<h2>Robots</h2>

 + __Robots with Stomp__
    + Changed:
        + Increased fall damage from 50 to 100
        + Reduced stomp damage from 500 to 125

+ __Disrutpors__
    + Removed:
        + Bonus coin generation
            + _Given how impactful disruptor suicide is and how it snowballs hard if done right, it's better to keep the economics of the robots steady. That way it's easier to count robot deaths to see how much coins they would have and add another element to master in this gamemode_


+ __Lister, Samwiz, Mechaface__
    + Removed:
        + Incorrect bonus coin on death

+ __Tone Techniciaon__
    + Added:
        + 3x capture rate
    + Changed:
        + Rocket Jumper:
            + Reduced reload penalty from -400% to -75% slower
        + Market Garden:
            + Increased damage bonus vs players from +25% to +40%

    + Removed:
        + Incorrect tank particle

+ __WAMO__
    + Changed:
        + Reduced uber duration on MMPHHF from 5 to 3 seconds

+ __Zonical__
    + Removed:
        + Larger dispenser size
            + _This was moved to a new robot instead_
+ __Elmaxo__
    + Changed:
        + Firerate bonus from +10% to +25% faster
        + Damange peanlty vs players from -15% to -25%
            + _This older design is more fair to fight for humans as the less damage makes it so he has to be closer to secure a kill, thus allowing for more counterplay for humans, this bot is still amazing at picking off lone humans with relative ease, and with these damage values, he struggles in larger groups of humans, making him a balanced robot_

<h2>New Robots</h2>

+ __Barricade__
    + Role: Engineer
    + HP: 1500 <img align="right" width="250" height="auto" src="https://www.bmod.tf/assets/images/mm/barricade.webp">
    + Move Speed: -25%
    + Weapons 
        + Shotgun:
            + +20% faster fire rate
            + -85% slower reload
            + Reloads full clip at once
            + -50% damage vs buildings
        + Southern Hospitality:
            + +25% damage bonus
            + +1000% faster construction speed
            + +100% repair rate bonus
            + -70% smaller sentry attack radius
            + -150% slower sentry firing speed
            + 10x larger dispenser radius
            + Sentry Health: 561
            + Sentry Scale: +200% bigger
            + Dispenser Health: 750
            + Dispenser Scale: +200% bigger
            + Dispenser starts at LVL 3
    + Cost: Free
    + Designed by: HiGPS, PizzaPasta
        + _This robot is intended to provide barricades and thus does not have a strong sentry_

<h2>Backend</h2>

+ Robot Engineer Attributes
    + Added:
        + Improved custom build levels on sentries

+ Crit vs Jumpers
    + Added:
        + Default value of 0 when critting

Do join the \[discord\](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the \[Steamgroup\](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play](2023-09-03-mm-update-66.md)