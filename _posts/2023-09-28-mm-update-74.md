--- 
title: "Barricade Buff"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---


<h2>Robots</h2>

+ __Barricade__
    + Removed:
        + -200% slower sentry firing speed penalty

+ __Kristianma__
    + Added:
        + Missing killstreak stat
    + Changed:
        + Airblast vertical vulnerability multiplier from +25% to +50% 
        + Weapon switch bonus from +95% to +25% faster
    + Removed:
        + Axtinguisher 175 heal on kill

+ __Archibolt__
    + Removed:
        + Dmg taken penalty





Do join the \[discord\](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the \[Steamgroup\](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play](2023-09-03-mm-update-66.md)