---
title: SAXTRON HALE Update is here!
author: Heavy Is GPS
categories: ["updates","mm"]
---

<h2>Gameplay</h2>

+ Added:
    + Custom content that will rely on the assetpack to be viewed properly. You can still play without it, but some aspects will have errors, and certain sounds will not play, and certain particle effects will be incorrect. Get the assetpack [here](https://www.bmod.tf/robot_animation_fixes_v4.zip)
    + Back button to !cr robot menu
    + Subclasses to robots to easier find the type you're after
    + Boss-Coins, boss coins are earned by completing objectives, Boss-Coins are shared on the team, meaning each team gets a max of 4 Boss-Coins per round.
+ Changed:
    + Robot-Coins are no longer shared among the team, it now functions as the MvM money does where you gather together but spent individually
    + Robot-Coins are set to 0 when switching teams
    + Reduced clutter in the robot menus


<h2>Human Balance</h2>

+ Frontier Justice
    + Every 600 damage dealt with the sentry adds 1 assist to your sentry
+ Scout
    + Bat & All-Class melee unlocks
        + Provides 50% more primary and secondary ammo
    + Wrap Assasin
        + Ornament stun duration reduced to 1.5 second and move speend penalty -70% move speed
    + Sandman
        + Baseball stun duration increased to 2.0 seconds at -85% move speed



<h2>Robot Balance</h2>

__Tanks__
+ Added: 
    + Tanks now use unique engine and spawn sounds to differenciate them from the non-tanks
    + Announcer will announce if there are more than 1 tank on the opposing team
    + If most of the robot team consists of tanks, a tank will play

__Engineers__
+ Added:
    + Ability to phase through their own buildings
        + _This gives them the bot behavior that MvM engineer bots have_
    + Announcer will say if there are multiple Engineer Robots
    + Announcer will say if teleporter is active upon the death of the Engineer Robot
+ Changed:
    + Increased metal regen from 150 to 200
    + Changed role from Support to Builder
    + Increased move speed penalty to -25%

__Team Porters (robot-teleporter exits)__
+ Changed:
    + Only charges if the robot team has a Robot-Engineer
+ Added:
    + Red Team Porter has red effect (only visible with assetpack)

__Robots With Sticky Bomb Launcher__
+ Added:
    + Proper walk animation when holding a sticky bomb launcher
        + _Note: only with assetpack_

__Uncle Dane__
+ Removed:
    + Builds lvl 3 buildings instantly
+ Added:
    + Instantly upgrades a buildings level in one swing
    + 20% faster firing speed on Sentry
+ Changed:
    + Reduced cost of sentries and dispensers to compensate

__Habib__
+ Removed: Removed the entire robot as it was just a meme that lasted way too long

__Stickler__
+  Changed
    + First Person view now uses sticky bomb launcher model

__Robo-Knight__
+ Changed:
    + Move speed penalty from -50% to -25% slower
    + Reduced health from 3300 to 2500
+ Removed:
    + Move speed penalty on Shield Charge

__Volcanyro__
+ Added:
    + Instant weapon switch
    + On Kill: Speed boost for 2 seconds
    + On Kill: Heal 175 hp
+ Changed:
    + Reduced health from 3000 to 2750
    + Increased movespeed penalty from -20% to -15%
    + Increased weapon swing speed from -20% to +15%

__Kristianma__
+ Added:
    + Instant Weapon switch
+ Changed:
    + Increased detonator fire speed bonus from +30% faster to 85% faster

__Loch'n Larry__
+ Changed:
    + Role from Anti-Sentry to Sentry Buster
    + Lowered damage penalty vs players from -25% to -10% less damage
    + Lowered reload speed penalty from -100% to -25 slower
+ Added:
    + 150% larger explosion radius
    + +3 clip size
+ Removed:
    + Sentry Damage Resistance

__Bursty__
+ Added:
    + Rocket Specialist attribute
    + Entire clip fires when fully reload
+ Changed:
    + Role from Anti-Sentry to Sentry Buster
    + Lowered damage penalty vs players from -25% to -10% less damage
+ Removed:
    - -1 clip size penalty

__ICBM / Artillery__
+ Changed:
    + Renamed ICBM to Artillery

__Lazy Purple__
+ Direct Hit
    + Removed:
        + -25% damage penalty
        + +25% blast radius
    + Added:
        + +20% faster firing speed
    + Changed:
        + Increased damage penalty vs buildings from -30% to -40%
        + Increased projectile speed to 160% faster

<h2>New Robots</h2>

__S4XTR0N H4L3__
+ Class: Saxton Hale
+ Role: Boss
+ HP: 7400
+ Movespeed +50% 360H/U (Using Soldier as base, he slightly faster than medic, but not faster than a scout)
+ Weapons
    + BARE HANDS
        + Deals 114 damage
+ Abilities
    + Rage Ability: Build rage by giving or taking damage, taunt to activate
        + Taunted enemies are scared and Saxtron Hale gets a temporary defense buff
    + Brave Jump: Charge jump by holding M2, look up to activate
    + Weight Down: Look down and crouch while being in the air for a bit to rapidly boost your downward momentum
+ Cost: 2 Boss-Coins + 2 Robot-Coins per robot in the team.
+ _Saxtron Hale plays close to what he does in Versus Saxton Hale gamemode_

__Shounic (rework)__
+ Class: Soldier
+ Role: Damage
+ Sub-Role: Rockets
+ HP: 3300
+ Movespeed +50% 360H/U (Using Soldier as base, he slightly faster than medic, but not faster than a scout)
+ Weapons
    + Rocket Launcher Launcher
        + Shoots rocket launchers that shoots rockets
        + Fired Rockets aim where Shounic is Aiming
        + Rockets deal reduced damage to players and buildings
+ Cost: Free
+ _I've always wanted this since I was a kid. A rocket launcher launcher_

__Fatmagic (rework)__
+ Class: Sniper
+ Role: Damage
+ Sub-Role: Melee
+ HP: 2250
+ Movespeed: Normal
+ Weapons
    + Instant weapon switch
    + Bushwacka
        + +20% faster swing speed
        + +75% more damage to players
        + -75% less damage to buildings
        + On Kill: 2 second speed boost
    + Jarate
        + +75% faster recharge
        + On coated in Jarate: Pissmaster, become ubered for 1 second
+ Cost: Free
+ _Fatmagic wanted this (I think?), but the uber on piss taken makes him a soft counter to sydney and jarate snipers somewhat_

__Skymin Slash (rework)__
+ Class: Medic
+ Role: Healer
+ Sub-Role: Medigun
+ HP: 2500
+ Movespeed: -40%
+ Weapons
    + Vaccinator
        + +15% Passive damage resists
        + 10% Active damage resists
        + On VacDeploy: Healer and Healtarget both get the reflect rune from mannpower
    + Solemnvow
        + +50% damage done
+ Cost: Free
+ _Skymin used to be the least picked and most boring medic robot. With this reflect ability, it should make Skymin a viable pick vs mass sentries or human teams that just spam a lot_

__Lister__
+ Class: Pyro
+ Role: Damage
+ Sub-Role: Flames
+ HP: 3000
+ Movespeed: Slow at first, then fast
+ Weapons
    + Dragon's Fury
        + +50% damage bonus vs players
    + Powerjack
        + 75% damage bonus vs players
        + 50% airblast push force
        + While Active:
            + +50% faster move speed
            + 60% more damage taken
        + On Kill: Heal 175hp
+ Cost: Free
    
__Mecha Face__
+ Class: Scout
+ Role: Damage
+ Sub-Role: Hitscan
+ HP: 2250
+ Movespeed: Slow at first, then fast
+ Weapons
    + Baby Face's Blaster
        + +75% tighter bullet spread
        + +25% slower firing speed
        + 10% slower reload speed
        + Hype is never lost
        + Hype only builds on kills
        + 100% damage bonus vs players
+ Cost: Free
+ _Fatmagic wanted this (I think?), but the uber on piss taken makes him a soft counter to sydney and jarate snipers somewhat_

__Carpet Bomber__
+ Class: Demoman
+ Role: Damage
+ Sub-Role: Grenades
+ HP: 3300
+ Movespeed: -50%
+ Weapons
    + Loose cannon
        + Explodes into smaller cannon balls
        + +50% damage vs players
        + +45% faster firing speed
        + +50% faster reload speed
        + 100% faster projectile speed
        + -40% damage vs buildings
        + 0.5 seconds longer fuse time
+ Cost: Free
_Demoman version of Ivory_


Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.
