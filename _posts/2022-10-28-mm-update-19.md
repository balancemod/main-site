---
title: "More Unlock Specialization for humans"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

<h2>Robot  Balance</h2>

__Nerfs__

+ __Jouleson__
    + Removed:
        + Ability to capture and contentest capture

+ __Ubermachine__
    + Changed:
        + Reduced move speed bonus from +25% to +10% faster
    + Removed:
        + Becoming ubercharged when attacked by an ubered player
            + _This was an experimental test to see if this would add some counterplay, but ended up being too punishing for medics who are literally doing what they're supposed to do_

+ __Rocketman__
    + Changed:
        + Reduced flame speed from 4140 to 3600 (same as Agro)
            + _The range this robot had on it's flamethrower was nutty_

+ __Laugh Extractor__
    + Changed:
        + Reduced damage bonus vs players from +150% to +75%
        + Reduced flame speed from 4140 to 3600 (same as Agro)

<h2>Human Balance</h2>

__Buffs__

+ __Sniper__
    + SMG
        + Added:
            + On hit: Slow robots for 40% for 0.1 seconds
                + _The slow is very minor and requires a constant stream of damage from the SMG to matter, giving it a similar utility as Jarate, we still need to see the impact this has in a real game._
    
    + Kukri
        + Added:
            + +15% movespeed while active
                + _It's now the knife from CS:GO_
    
    + Shahansah
        + Changed:
            + Increased damage bonus while dead to +200%
                + _With the ease of bushwacka and the abudane of mini-crit sources, this weapon now does roughly the same damage as the bushwacka when below half health without any buffs_

    + Classic
        + Added:
            + Headshots at any charge
                + _Ambassador for the Sniper, but with a scope and more damage_

    + Tribalmans Shiv
        + Changed:
            + Increased bleed duration to 20 seconds
                + _This is now a really good spy tagger, in case there are noy pyro's on the team, not as effective as fire, but OK none the less_

+ __Scout__
    + Bat
        + Added:
            + +25% faster reload speed on all guns
                + _This should give the scout some more utility as you are no longer a mini-crit source_

    + Atomizer
        + Added:
            + 6 Bonus Jumps
                + _Having this many jumps makes jumping and mini-critting robots a viable strat_

+ __Spy__
    + Revolver
        + Added:
            + Tags last enemy hit for 5 seconds
    
    + Enforcer:
        + Added:
            + +200% damage bonus to sapped buildings


__Nerfs__

+ __Heavy__
    + Miniguns
        + Added:
            + -20% damage penalty to Robots
                + _Heavies is the easiest class to do damage, and since the robots are so big, the range of which all pellets hit is pretty big thus increasing the heavies damage to more than it is in regular TF2 due to the size difference in target. Stacking heavies was very easy and combined with mini-crits and mad milk, some heavies was just unkillable for robots since they had nutty sustain, this change means that the heavy has to spend roughly 30 more ammo to make up for the -20% damage, which isn't too bad_