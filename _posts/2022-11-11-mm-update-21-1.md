---
title: "Robot Buffs and tweaks"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

<h2>Human Balance</h2>

+ Medic
    + Solem Vow:
        + Increased healing mastery from 1 to 4 points in terms of upgrade

<h2>Robot Categories</h2>

+ Removed:
    + Disruptor sub categories, disruptors are now capped at 6 per category.
    
+ Changed:
    + Paid disruptors all generate 1 bonus RC on death

<h2>Robot Balance</h2>
    
+ __Bonk Scout__
    + Changed:
        + Fixed incorrect damage stat being applied making baseballs deal too much damage. Baseballs now deals the correct 40 damage instead of 100

+ __Batman renamed to Grapple Hook__
    + Changed:
        + Robot type from MK I to MK II
            + Cost: 1 RC per robot
        + Increased health from 1250 to 1750
        + Reduced damage penalty on butter knives from -75% to -25%
        
+ __Shogun__
    + Changed:
        + Robot type from MK I to MK II
            + Cost: 1 RC per robot
        + Increased health percentage restored on kill from 10% to 20% of total health

+ __Fan Scout__
    + Changed:
        + Robot type from MK I to MK II
            + Cost: 1 RC per robot
        + Increased health from 1250 to 2150
        + Reload speed from -10% slower to +20% faster

+ __Archibolt__
    + Changed:
        + Robot type from MK I to MK II
            + Cost: 1 RC per robot
        + Increased health from 2500 to 3500

+ __Hammerhead__
    + Changed:
        + Increased health from 1250 to 2000

+ __Firewall__
    + Changed:
        + Burning players mini-crit to crit
        + Reduced damage penalty vs burning players from -10% to -50%
            + _This is to offset the damage bonus you get from overheat, and so that you don't instantly melt with the fire ring_

+ __Megaton__
    + Changed:
        + Reduced vertical knockback on hit from 650 to 350 forcepowers

+ __Lazy Purple__
    + Changed:
        + Robot type from MK I to MK II
            + Cost: 1 RC per robot
        + Reduced movespeed penalty from -20% to -15% (now same as Elmaxo)
        + Reduced blast radius penalty from -80% to -20%
        + Faster projectile speed back to default Direct Hit
        + Reduced fire rate bonus from +20% to +10% faster
    + Removed:
        + Damage penalty vs buildings
        
+ __Renamed Gotham Bat Droid -> Gotham Attack Droid__
    + Added:
        + Better description in the menu for this bot

+ __Robo Knight__
    + Added:
        + 5 second Mini-crit on kill
        + +10% faster attack speed
        + Full charge turn control
    + Removed:
        + +35% Jump height bonus
    + _This robot keeps bouncing between OP and trash, this iteration is most likely OP_

+ __Dreysidel__
    + Changed:
        + Movespeed penalty from -50% to -35% slower

+ __Wamo__
    + Added:
        + Activating MMPHF grants an additional 5 seconds of uber

+ __Necromancer__
    + Changed:
        + Reduced kills needed for ability from 3 to 2

+ __Laugh Extractor__
    + Added:
        + Spooky noise when activating the ability

+ __StabbyStabby__
    + Added:
        + Missing spy alert spawn sound


<h2>Optimization</h2>
    
+ Optimized some of the medic code

<h2>Bugfixes</h2>

+  Added code to ensure clip sizes are correctly applied to robots having less clip than standard