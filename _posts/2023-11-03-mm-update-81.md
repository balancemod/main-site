---
title: "Blood Bandit joins the roster"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

## Humans

+ __Soldier__
    + Liberty Launcher
        + Added:
            + Bison: +35% faster reload speed
            + Base Jumper: +25 max hp bonus
            + _These were the only two weapons the liberty launcher didn't buff in some capacity_

    + Equalizer
        + Added:
            + On Hit: +20% bonus rage to banners

## Robots

+ __Nuker__
    + Fixed:
        + Nuker's bombs not using the payload model

+ __Sentry Buster__
    + Changed:
        + Method of explosion to be that of a tf_generic_bomb instead of manual applied damage in a sphere
            + _This should make it more consistent with geometry, you can still hide behind buildings to avoid getting killed_

+ __Ivory__
    + Fixed:
        + MIRV rockets now inherit stats from the original rocket launcher correctly
    + Changed:
        + Converge Mode: MIRV rockets moves towards where the player was aiming during activation
        + Reduced damage penalty vs players to -20%
        + Increased damage penalty vs buildings from -50% to -25% _to compensate for 4 rockets_
        + Reduced reload speed penalty from -200% to -100% slower
    + Removed:
        + -15% slower fire rate penalty

+ __Chat GPT Guardion__
    + Removed:
        + Short Circuit
    + Changed:
        + Frontier Justice -> Panic Attack
            + **Attributes**
                + "dmg bonus vs players" "1.25"
                + "clip bonus penalty" "1.6"
                + "killstreak tier" "1.0"
                + "engineer building teleporting pickup" "10.0"
                + "dmg penalty vs buildings" "0.5"
        + Eureka Effect -> Wrench
        + Health from 1900 to 2000
        + Scale from 1.55 to 1.65
        + Move Speed Penalty from -22% to -25% slower
        + Rest of robot stats to match other paid engineers

+ __SAM -> Samureye__
    + Renamed and changed cosmetics for better visibility and theme to the robot

+ __Exploder__
    + Fixed:
        + Grenade Launched bombs not using the correct team color

## New Robots
+ __Blood Bandit__
    + Role: Healer
    + Class Medic
    + HP: 2000 <img align="right" width="250" height="auto" src="https://www.bmod.tf/assets/images/mm/bloodbandit.webp">
    + **Player Attributes**:
        + "move speed penalty" "0.8"
        + "damage force reduction" "0.8"
        + "airblast vulnerability multiplier" "1.8"
        + "cancel falling damage" "1.0"
        + "health regen" "20.0"
        + "head scale" "0.75"
        + "rage giving scale" "0.85"
    + **Weapons**
        + __Overdose__
            + **Attributes**
            + "killstreak tier" "1.0"
            + "fire rate bonus" "0.75"

        + __Medigun__
            + **Attributes**
            + "killstreak tier" "1.0"
            + "overheal penalty" "0.01"
            + "ubercharge rate penalty" "1.6"
            + "heal rate bonus" "2.0"
            + **custom_attributes_weapon**
                + "medigun charge is cond" "cond=94 duration=6.0"
    + COST: 2RC per robot
    + Designed by: [Pizza Pasta](https://steamcommunity.com/profiles/76561199125109082/)

+ __Phase Bomber__
    + Role: Damage - Grenades
    + Class Damage
    + HP: 3500 <img align="right" width="250" height="auto" src="https://www.bmod.tf/assets/images/mm/phasebomber.webp">
        + **Player Attributes**:
            + "damage force reduction" "0.5"
            + "move speed penalty" "0.9"
            + "airblast vulnerability multiplier" "0.8"
            + "cancel falling damage" "1.0"
            + "charge impact damage increased" "1.5"
            + "ammo regen" "100.0"
            + "rage giving scale" "0.85"
            + "self dmg push force increased" "2.0"
            + "charge time increased" "4.0"
            + "head scale" "0.75"
        + **custom_attributes_player**
		    + "OnCond-SpawnBomb" "oncond=17"

        + **Weapons**
		+ __Crossing Guard__
            + "killstreak tier" "1.0"
            + "charge time increased" "5.0"
            + "dmg penalty vs players" "1.25"
        + __Tide Turner__
			+ "major increased jump height" "1.5"
			+ "lose demo charge on damage when charging" "0.0"
    + COST: 2RC per robot
    + Designed by: Chocy milk addict


## Backend  
+ __Colored Weapons__
    + Removed debug message shown whenever a robot with a colored weapon spawned

+ __Medigun is condition attribute__
    + Created a custom attribute that replaces uber with whatever condition is chosen on the medic and heal target

+ __Config__
    + Changed the "paint" string in weapons to be "warpaint_id" to better reflect what it is.

Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play
