---
title: "The Engineer Bot Nerfs"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

## Robots

+ __Paid Robot Engineers__
    + Changed:
        + Reduced Base HP from 3000 to 2250
        + Reduced health per player from 60 to 30

+ __Free Robot Engineers__
    + Changed:
        + Reduced health per player from 60 to 20

+ __NoFungineer__
    + Remote Sentry Control:
        + Changed:
            + Reduced movement from 800 to 680 speed units
            + Increased fall gravity from 50 to 600
         
            

+ Join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!
+ Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.
+ [Human Buffs Wiki Page](https://wiki.bmod.tf/Manned_Machines_Human_Buffs)
+ [Robots Wiki Page](https://wiki.bmod.tf/Robot_Overview)

Use the !seed command on our servers to let the discord server know you want to play
