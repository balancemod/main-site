--- 
title: "Update Number 69"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

<h2>Humans</h2>

+ __Soldier & Demoman__
    + Half Zatoichi:
        + Changed:
            + Increased health on hit from 15 to 25
                + _35 was too much, 15 was too little, maybe 25 is just right_

+ __Sniper__
    + Huntsman:
        + Added:
            + +20% faster firing speed

+ __Pyro__
    + Back Scratcher
        + Added:
            + 20 second bleed on hit
        + Removed:
            + Healing penalty from healers

<h2>Robots</h2>

+ __Stealthed Robots__
    + Added:
        + Electric weapons removes stealth on hit
    + Changed:
        + Bleed now removes stealth on each bleed tick on hit
    + Removed:
        + Fire no longer removes stealth
    + _This only applies to the Mr Paladin and Cloaker stealth cloak, regular cloak is unaffected by this_

+ __B4nny__
    + Removed:
        + Doing damage yields damage bonus buff

+ __Zonical__
    + Changed:
        + Reduced switch duration bonus from 1.5 to 1 second
        + Increased damage bonus vs players from +125% to +200%
        + Reduced fire rate penalty from -150% to -300% slower
        + Removed faster reload speed bonus
        + _This change makes it so long range non-crit dmg will be 38 per shot, and min-crit shots will be 71 up close 61 long range. And only the first 3 shots if you hold attack button down will be mini-crit. This should make it more rewarding considering how hard this robot is to play_

+ __Skymin__
    + Changed:
        + Increased uber charge rate penalty from -15% to -25% slower
            + _The latest change with longer duration and faster build rate med the vaccinator charges a little too common, this reduction should make there be more time between each vaccinator charge without it crippling the robot's power_

+ __Dr Crossbow Cop__
    + Changed:
        + Increased heal bolts needed for buff from 10 12
        + Reduced clip size from 3 to 2
        + _These changes made Crossbow cop way too easy and good compared to the effort needed, as he was fine how he was originally, just not played that much_

+ __Carbine Cassidy__
    + Added:
        + +102.5% damage bonus vs players
    + Changed:
        + Reduced headshot damage from +200% to +100%
    + _This change makes hitting all 4 shots in volley the following: 4xBodyshot=52 at max range.4xHeadshot=386 4xMini-critBodyShot=196. With these changes the reward is larger to match the difficulty of this robot_

+ __Crocotron__
    + Changed:
        + Bushwacka to Kukri

<h2>Backend</h2>

+ MvM upgrade station stats:
    + Robots: Touching Resupply no longer brings up the menu

Do join the \[discord\](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the \[Steamgroup\](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play](2023-09-03-mm-update-66.md)