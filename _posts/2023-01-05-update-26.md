---
title: "Fan O' War Buff"
author: Heavy Is GPS
categories: ["updates","bmod"]
---

<h2>Bugfix</h2>

__Fan O' War__
+ Added:
    + This weapon deploys and holsters +40% faster
        + _Suggested by Big Stick_

Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play

