---
title: "Mini Sentry Buster and tank tweaks"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---
<h2>Attributes</h2>

+ __Added custom attributes for:__
    + Faster Respawn
    + Being A Sentry Buster
        + _These changes should have little impact for the average player, this is more stuff for future scaling and compatibility with other mods_

<h2>Robot Balance</h2>

+ __LED__
    + Changed:
        + Increased clip size bonus from 50% to 200%

+ __Robo Knight__
    + Removed:
        + Kill refills meter
            + _Robo knight remains difficult to balance, removing the refill on kill reduces the feeding potential bad players can do to him and the devestation he causes as a result_

+ __Demopan__
    + Changed:
        + Reduced jump height bonus from +50% to +12.5%

+ __Solar Light__
    + Changed:
        + Increased damage penalty vs buildings from -50% to -75%  
            + _Solar Light Was a little too good against buildings for being a free and easy robot_
+ __Tanks__
    + Removed:
        + Health on hit
        + Health on kill
    + Changed:
        + Increased health of each tank to compensate for lack of sustain
    + _Tanks being tanky and sustaining health made it hard for humans to determine how much more a tank could take. We will not be giving tanks on hit or on kill healing abilities in the future, but if tanks get shredded we can always increase their health to compensate, they are tanky after all_

+ __Sentry Buster__
    + Changed:
        + Fixed some issues with the line of sight detonation (still not perfect)
        + Reduced arm time from 2 seconds to 1.25 seconds (fast exploder now)

<h2>New Robots</h2>

+ __Sentry Buster Mini__
    + Cheaper and smaller than normal sentry buster
    + Faster detonation time
    + Less Explosion Radius
    + Less Damage done (will still one shot buildings)
    + No high jump
    + Cost: 1 RC
        + _This is a test bot for testing the new attributes for sentry busters_