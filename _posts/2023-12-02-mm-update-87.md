---
title: "Barricade Nerf and Tank Damage Building Nerf"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---


## Robots

+ __Tanks__
    + Changed:
        + Reduced damage penalty to buildings from -20% to -65%
            + _It's way too easy for a tank to just walk up and destroy buildings_

+ __Barricade__
    + Changed:
        + Reduced repair rate from +300% to +100% more effective

Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play
