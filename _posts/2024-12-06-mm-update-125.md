---
title: "Barricade tweak and system improvements"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

## Robots

+ __Barricade__
    + Changed:
        + Reduced sentry size from 150% to 100% bigger
            + _Now sappers will properly attach to it_

+ __Black Shiv__
    + Removed:
        + 200% more health from healthpacks
        + +45% faster swing speed
        + -25% damage penalty
    + Fixed:
        + Fixed coin generating logic to not award robot coins on deadringer death
    + _Black Shiv had a lot of balance left over from prior to getting the deadringer. These changes will make him less of an unkillable coin generating knightmare that goes Ham_

## Gameplay Systems

+ __Volunteering__
    + Changing teams after the mode has started will now volunteer and unvolunteer as a robot.

+ __Robot Creation__
    + Added logic to remove robots from selection that gets removed the roster during server uptime.
    + Added logic to prevent blank slate robots where you keep your personal loadout on robots that are removed.

+ Join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!
+ Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.
+ [Human Buffs Wiki Page](https://wiki.bmod.tf/Manned_Machines_Human_Buffs)
+ [Robots Wiki Page](https://wiki.bmod.tf/Robot_Overview)

Use the !seed command on our servers to let the discord server know you want to play
