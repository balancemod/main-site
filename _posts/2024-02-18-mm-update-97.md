---
title: "The Inflation Update"
author: "Heavy Is GPS"
categories: ["updates","mm"]
---

## Humans
+ __Electric Weapons__
    + Changed:
        + Reduced healing recieved debuff applied on hit from -50% to -20%
            + _The effectiveness of one player on a robot medic was too strong and scaled way too good in humans favor_

## Robots

+ __Robots__
    + Changed:
        + All robots costs x10 more RC.
        + RC generated on death increased from 1 to 10
        + Minor adjustments to many robots cost to account for the inflated price

+ __Warper__
    + Added:
        + Missing footstep sounds

+ __Uber Flasher -> Immortality Protocol__
    + Changed:
        + Uber to Death Negating attribute
            + _This means this uber is silent, and has no effect, but the targets health don't go below 1 HP_
    + Added:
        + 400% overheal bonus
            + _This overheal bonus is based on the class, not their max health, meaning all heavies get the same bonus of 750 hp_
+ __Samwiz1__
    + Fixed:
        + Not playing explosion sound on death

## Backend

+ __Last enemy hit attribute__
    + Added some logic to prevent error spamming whenever a player would take fall damage

Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play
