---
title: "Updating Robot Descriptions & Tips"
author: PizzaPasta
categories: ["updates","mm"]
---

<h2>Important Changes</h2>

+ __All Tanks__
    + Added:
        + "You can't contest objectives" to each tank that didn't have the message in tips

+ __All Robot Pyros w/ Airblast Heals__
    + Added:
        + Now shows how much health you heal per extinguish in tips

+ __Mecha Face__
    + Changed:
        + Description and tips now properly reflect the robot

+ __Rocket Man__
    + Fixed:
        + Description no longer takes up two lines on the robot select menu

+ __Solarlight__
    + Fixed:
        + Changed name to remove the annoying space between "Solar" and "light"
            + _It really annoyed me that much. -PizzaPasta_

+ __BazookaJoe2002__
    + Added:
        + Now has proper robot tips

+ __Other description and/or tip changes for the following bots__

    + Big Joey, HiGPS, Rocket Man, Volcanyro, Hoovy, Funke, Uncle Dane, Wrangler Bill, Fatmagic, Carbine Cassidy, Equalizor, Array Seven, Crockotron, Robo-Knight, Solarlight, Bearded Expense, Laugh Extractor, Sultan, Bursty, Jouleson, Rager, Ubermachine, Hacked Slasher, Nofungineer, Quick-Shielder, Blastinator, Pilgrim, Saxtron H4l3, Demopan, and Sergeant Stickler

    + Changes include improved wording, adding some advice, and cleaning up out-of-date stats

Do join the [discord](https://discord.gg/VMAJgEJ) if you haven't already!!

Join the [Steamgroup](https://steamcommunity.com/groups/higps) for pings and announcements for when we play.

Use the !seed command on our servers to let the discord server know you want to play