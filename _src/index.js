import { createServer } from "node:http";
import { dirname, join } from "node:path";
import express from "express";

// Routers
import eleventy from "./engines/eleventy.js";
import api from "./routers/api/index.js";
import play from "./routers/play.js";

// Middleware
import { fileURLToPath } from "node:url";
import cors from "cors";
import { newLogger } from "./util/logger.js";

// Engine
const engines = {
	eleventy,
	api,
	play,
};

const LOG = newLogger("WebServer");
const app = express();
const server = createServer(app);

// Settings
app.set("json spaces", 2);

// Enable CORS
app.use(cors());
app.use((_req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept",
	);
	next();
});

async function main() {
	//LOG("Setting up");

	const __filename = fileURLToPath(import.meta.url);
	const __dirname = dirname(__filename);

	const baseDir = join(__dirname, "..");
	const routers = {};
	for (const name in engines) {
		routers[name] =
			(await engines[name](
				"jekyll" in routers ? routers.jekyll.site : baseDir,
			)) || {};
	}
	//const jekyll = routers.jekyll;
	//setupSocket(server,jekyll.site)

	// Routers
	LOG("Loading routers");
	for (const name in routers) {
		"router" in routers[name] && app.use(routers[name].router);
	}

	LOG("Starting the webserver");
	const port = process.env.PORT || 3000;
	server.listen(port, () => {
		LOG(`Server listening on port ${port}`);
	});

	console.log(`
Build Stage: ${"n/a"}
Source: ${baseDir}
Site URL: ${"n/a"}

Ctrl+C to stop`);
}

main();
