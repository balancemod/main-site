import Eleventy from "@11ty/eleventy";
import { Router, static as expressStatic } from "express";

export default async function init() {
	if (process.env.BMOD_BUILD_STATIC) {
		const eleventy = new Eleventy();
		await eleventy.init();
		await eleventy.watch();
	}
	const router = Router();
	if (process.env.BMOD_SERVE_STATIC) {
		router.use(expressStatic(process.env.STATIC || "_site"));
	}
	return {
		router,
		site: {},
	};
}
