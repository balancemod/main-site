import { newLogger } from "./logger.js";
import { getTime } from "./timer.js";

const queuelist = {};
const delay = 0.5;
const LOG = newLogger("Queue", null, false);
let isTickRunning = false;

let next;
let interval;

function getNext() {
	const ref = Object.keys(queuelist)[0];
	if (!ref) {
		return undefined;
	}
	const thing = queuelist[ref];
	delete queuelist[ref];
	return thing;
}

async function tick() {
	if (isTickRunning) {
		return; // Skip if already running
	}
	isTickRunning = true;

	try {
		const time = getTime();
		if (!next || time >= next) {
			LOG("Tick", `(${Object.keys(queuelist).length} things left)`);
			next = time + delay;
			const fn = getNext();
			if (fn !== undefined && typeof fn === "function") {
				await fn();
			}
		}
		if (!Object.keys(queuelist).length) {
			stop();
		}
	} catch (err) {
		console.error("Error in queue", err);
	} finally {
		isTickRunning = false;
	}
}

function start() {
	LOG("Starting queue");
	interval = setInterval(tick, delay * 1000);
}

function stop() {
	LOG("Ending queue");
	clearInterval(interval);
	interval = null;
	LOG.logout();
}

export function queue(ref = btoa(Math.random())) {
	return new Promise(res => {
		if (!interval) {
			start();
		}
		if (!queuelist[ref]) {
			LOG("Adding function to queue");
			queuelist[ref] = res;
		}
	});
}
