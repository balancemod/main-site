import { queryGameServerInfo } from "steam-server-query";
import { queue } from "./queue.js";
import { newLogger } from "./logger.js";
import { cache } from "./cache.js";
import servers from "../../_data/servers.json" with { type: "json" };
import _ from "lodash";

const LOG = newLogger("Servers");
const serverIps = Object.keys(servers);

function extractInfoFromName(name) {
	if (typeof name !== "string") {
		return {};
	}
	const extraInfo =
		/^(?<host>[^|]*) \| (?<region>[\w ]*)(?: #(?<regionId>[\d]*))?(?: \| (?<mode>[^\|]*))?(?: \| (?<domain>[^\|]*))?$/gm.exec(
			name || "",
		);
	return extraInfo;
}

export async function getServer(ip) {
	return await cache(`server-${ip}`, async (server = { ip }) => {
		LOG(`Getting server ${ip}`);
		await queue(`server-${ip}`);

		const knownInfo = servers[ip];
		if (knownInfo) {
			server = _.merge(server, knownInfo);
		}
		server.query = server.query || ip;
		server.connect = server.connect || ip;

		try {
			const newData = await queryGameServerInfo(server.query, 1, 10000);
			if (newData) {
				server = _.merge(server, newData);
			}
		} catch (error) {
			LOG.error(`Error getting server (${server.query}): ${error}`);
			try {
				const newData = await queryGameServerInfo(server.ip, 1, 10000);
				if (newData) {
					server = _.merge(server, newData);
				}
			} catch (error) {
				LOG.error(`Error getting server (${server.ip}): ${error}`);
			}
		}

		const extraInfo = extractInfoFromName(server.name);
		if (extraInfo) {
			server = _.merge(server, extraInfo.groups);
		}

		if (server.region) {
			server.shortregion =
				{
					Poland: "pl",
					Singapore: "sg",
					Quebec: "ca",
				}[server.region] || server.region;
		}
		if (server.gameId) {
			delete server.gameId;
		}
		if (server.mode) {
			server.mode = server.mode.trim();
		}
		server.shortmode = shortenMode(server.mode);
		if (server.map) {
			server.shortgamemode = server.map.split("_")[0];
			server.gamemode = getGamemode(server.shortgamemode);
		}
		if (server.shortmode || server.shortgamemode) {
			server.tags = [server.shortmode, server.shortgamemode].filter(
				Boolean,
			);
		}
		return server;
	});
}

export async function getAllServerOptions() {
	const servers = await Promise.all(serverIps.map(getServer));
	const gamemodes = new Set();
	const regions = new Set();
	const modes = new Set();
	const maps = new Set();

	servers.forEach(server => {
		if (server.gamemode) {
			gamemodes.add(server.gamemode);
		}
		if (server.region) {
			regions.add(server.region);
		}
		if (server.mode) {
			modes.add(server.mode);
		}
		if (server.map) {
			maps.add(server.map);
		}
	});

	return {
		gamemode: Array.from(gamemodes),
		region: Array.from(regions),
		mode: Array.from(modes),
		map: Array.from(maps),
	};
}

export async function getServersUpdate() {
	const servers = await Promise.all(serverIps.map(getServer));
	return servers
		.filter(Boolean)
		.map(({ ip, map, players, maxPlayers, mode, gamemode }) => ({
			ip,
			map,
			players,
			maxPlayers,
			mode,
			gamemode,
		}));
}

export async function getTotalPlayerCount() {
	LOG("Setting total player Count");
	const servers = await Promise.all(serverIps.map(getServer));
	const serverPlayerCounts = servers
		.map(pc => (pc ? pc.players : 0))
		.filter(a => !!a);

	console.log({ serverPlayerCounts });

	return {
		players: serverPlayerCounts.reduce((a, b) => a + b, 0),
		servers: serverPlayerCounts.filter(Boolean).length,
	};
}

function shortenMode(mode) {
	return (
		{
			"Standard Map Rotation": "BMod",
			"Manned Machines - Giant Robot PvP": "MM",
			"2fort 24/7": "2Fort",
		}[mode] || mode
	);
}

function getGamemode(shortgamemode) {
	return (
		{
			arena: "Arena",
			ctf: "Capture the Flag / Mannpower",
			cp: "Control Point",
			koth: "King of the Hill",
			mvm: "Mann vs. Machine",
			pass: "PASS Time",
			pl: "Payload",
			plr: "Payload Race",
			pd: "Player Destruction",
			rd: "Robot Destruction",
			sd: "Special Delivery",
			tc: "Territorial Control",
			tr: "Training Mode",
			vsh: "Versus Saxton Hale",
			zi: "Zombie Infection",
		}[shortgamemode] || shortgamemode
	);
}

export async function getMostPopularServers() {
	const ip = await cache("popularServer", async () => {
		LOG("Getting Most popular Server");
		const servers = await Promise.all(serverIps.map(getServer));
		const filteredServers = servers
			.filter(a => !!a)
			.sort((a, b) => b.players - a.players)
			.filter(a => a.players < a.maxPlayers);
		if (filteredServers.length) {
			return filteredServers.slice(0, 3);
		}
	});
	return ip;
}

export async function filterServers(filterParams = {}) {
	const { sort, sortDir, ...filter } = filterParams;
	const servers = (await Promise.all(serverIps.map(getServer))).filter(
		Boolean,
	);
	const filterSpecified = !!Object.keys(filter).length;

	return servers
		.filter(server => {
			if (!filterSpecified) {
				return true;
			}
			const serverDetailed = Object.keys(server).length > 5;
			if (!serverDetailed && filterSpecified) {
				return false;
			}

			let go = true;
			const filterEntries = Object.entries(filter);
			for (const [key, value] of filterEntries) {
				go &=
					{
						full:
							filterParams.full === "false"
								? server.players < server.maxPlayers
								: server.players === server.maxPlayers,
						empty:
							filterParams.empty === "false"
								? server.players > 0
								: server.players === 0,
					}[key] ||
					server[key]?.toString().toLowerCase() ===
						value.toString().toLowerCase();
			}

			return go;
		})
		.toSorted((a, b) => {
			if (!sort) {
				return 0;
			}
			const aSort = a[sort];
			const sortResult = [aSort, b[sort]].sort()[0] == aSort ? -1 : 1;
			return (
				{
					asc: sortResult,
					desc: -sortResult,
				}[sortDir] || sortResult
			);
		});
}
