import frontmatter from "@github-docs/frontmatter";
import { lstatSync } from "node:fs";
import { readdir, readFile, stat } from "node:fs/promises";
import { basename, extname, join } from "node:path";
import { cache } from "./cache.js";
import { newLogger } from "./logger.js";

const LOG = newLogger("File Manager");

export async function getFile(file) {
	return await cache(
		`file-${file}`,
		async () => {
			LOG(`Reading file ${file}`);
			try {
				return (await readFile(file)).toString();
			} catch (error) {
				LOG.error(`Error reading file ${file}`, error);
				return "";
			}
		},
		false,
		true,
	);
}

export async function getFolder(folder) {
	LOG(`Parsing folder ${folder}`);
	try {
		return await readdir(folder);
	} catch (error) {
		LOG.error(`Error reading folder ${folder}`, error);
		return [];
	}
}

function parseFM(contents, file) {
	LOG(`Parsing FrontMatter of file ${file}`);
	let fm = {};
	let err;
	try {
		fm = frontmatter(contents);
		err = fm.errors.length;
	} catch (error) {
		err = error;
	}
	if (err) {
		LOG.error(`Error parsing frontmatter in ${file}`, err);
	}
	fm.data = fm.data || {};
	fm.data.raw = fm.content || "";
	return fm.data;
}

export async function loadDataFile(file, parser) {
	LOG(`Loading ${parser.name} file ${file}`);
	const contents = await getFile(file);
	return contents ? parser(contents) : {};
}

export async function loadFile(file) {
	LOG(`Loading file ${file}`);
	const contents = (await getFile(file)) || "";
	let data = {};
	if (contents) {
		try {
			data = parseFM(contents, file) || {};
		} catch (error) {
			LOG.error(`Error loading file ${file}`, error);
		}
	}
	LOG(`Applying information like (path,slug,filename) to ${file}`);
	data.path = file;
	const slug = basename(file).split(".");
	slug.pop();
	data.slug = slug.join(".");
	data.filename = basename(file);
	return data;
}

export async function loadYML(file) {
	return loadDataFile(file, contents =>
		parseFM(`---\n${contents}\n---`, file),
	);
}

export async function loadJSON(file) {
	return loadDataFile(file, contents => JSON.parse(contents));
}

export async function getStaticFile(file, basedir = null) {
	const static_file = {
		name: basename(file),
		extname: extname(file),
	};
	return {
		...static_file,
		path: basedir ? file.replace(basedir, "") : file,
		modified_time: (await stat(file)).mtime,
		basename: static_file.name.substring(
			0,
			static_file.name.length - static_file.extname.length,
		),
	};
}

export async function loadFolder(folder, filter) {
	LOG("Loading folder", folder);
	let files = await getFolder(folder);
	const data = [];
	files = files.filter(file => filter(file, folder));

	for (const file of files) {
		const filePath = join(folder, file);
		if (lstatSync(filePath).isDirectory()) {
			data.push(...(await loadFolder(filePath, filter)));
		} else {
			data.push(await loadFile(filePath));
		}
	}
	return data;
}

export async function loadFolderYML(folder) {
	LOG("Loading folder of YML files", folder);
	const files = await getFolder(folder);
	const data = {};

	for (const file of files) {
		const filePath = join(folder, file);

		if (lstatSync(filePath).isDirectory()) {
			data[basename(file, extname(file))] = await loadFolderYML(
				join(folder, file),
			);
		} else {
			switch (extname(file)) {
				case ".json":
					data[basename(file, extname(file))] = await loadJSON(
						join(folder, file),
					);
					break;
				case ".yml":
					data[basename(file, extname(file))] = await loadYML(
						join(folder, file),
					);
					break;
			}
		}
	}
	return data;
}

export function getPermalink(pl) {
	LOG("getting permalink for", pl);
	return (
		{
			date: "/:collection/:dir/:categories/:year/:month/:day/:title:output_ext",
			pretty: "/:collection/:dir/:categories/:year/:month/:day/:title:output_ext",
			ordinal:
				"/:collection/:dir/:categories/:year/:y_day/:title:output_ext",
			weekdate:
				"/:collection/:dir/:categories/:year/W:week/:short_day/:title:output_ext",
			none: "/:collection/:dir/:categories/:title:output_ext",
		}[pl] || pl
	);
}
