import { newLogger } from "./logger.js";

const timers = [];
const timer_groups = [];
const LOG = newLogger("Timer");

function roundTime(time) {
	return Math.round(time) / 1000;
}

export function getTime() {
	return new Date().getTime();
}

/**
 *
 * @param {T[]} array
 * @param {T} value
 * @returns {number}
 */
function arrayAdd(array, value) {
	let i = array.findIndex(v => v == null);
	if (i === -1) {
		i = array.push(value) - 1;
	} else {
		array[i] = value;
	}
	return i;
}

export function start_timer() {
	const i = arrayAdd(timers, getTime());
	LOG(`Starting new timer (${i})`);
	return i;
}

/**
 *
 * @param {number} i
 * @param {number} tg
 * @param {string} id
 * @returns {number}
 */
export function end_timer(i, tg, id) {
	LOG(`Ending timer (${i})`, tg && id ? `${id} from group ${tg}` : "");
	if (timers[i]) {
		const duration = getTime() - timers[i];
		timers[i] = null;
		if (tg !== undefined) {
			const timer_group = timer_groups[tg];
			arrayAdd(timer_group, { id, duration });
		}
		return duration;
	}
}

export function timer_group() {
	const tg = arrayAdd(timer_groups, []);
	LOG(`Starting new timer group ${tg}`);
	return tg;
}

/**
 *
 * @param {number} tg
 * @param {number} tooLong
 * @returns {{ longest: string[], sum: number }}
 */
export function close_timer_group(tg, tooLong) {
	LOG(`Closing timer group ${tg}`);
	const timer_group = timer_groups[tg];
	const longest = timer_group
		.filter(({ duration }) => duration > tooLong)
		.map(f => `${f.id} (${f.duration}s)`);
	const sum = timer_group.reduce((a, { duration }) => a + duration, 0);
	timer_groups[tg] = null;
	return { longest, sum: roundTime(sum * 1000) };
}
