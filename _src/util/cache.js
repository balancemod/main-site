import { deserialize, serialize } from "superjson";
import { newLogger } from "./logger.js";
import { queue } from "./queue.js";
import { getTime } from "./timer.js";
import { readFile, writeFile } from "node:fs/promises";

let cached = {};
const defaultSpan = 30000;
const cacheFile = process.env.BMOD_CACHE || "_src/cache.json";
const LOG = newLogger("Cache", null, false);

async function saveCache(data) {
	await queue("cache-save");
	LOG("Saving to file");
	await writeFile(cacheFile, JSON.stringify(serialize(data), null, 2));
}

async function cleanCache() {
	await queue("cache-clean");
	LOG("Cleaning cache");
	const time = getTime();
	for (const id in cached) {
		if (time > cached[id].expiry) {
			delete cached[id];
		}
	}
}

/**
 * @template T
 * @param {string} id
 * @param {() => T} get
 * @param {{ span?: number; force?: boolean }} options
 * @returns {Promise<T>}
 */
export async function cache(
	id,
	get,
	{ span = defaultSpan, force = false } = {},
) {
	const time = getTime();

	let currentCache = cached[id] || {};
	if (!currentCache || typeof currentCache !== "object") {
		LOG(`New cache "${id}"`);
		currentCache = { data: undefined, timestamp: 0 };
	}
	let data = currentCache.data;
	if (force || !data || time > currentCache.expiry) {
		currentCache.expiry = time + span;
		LOG(
			`Retrieving information "${id}" ${force ? "(forced)" : "(expired)"}`,
		);
		data = await get(data);
		currentCache.data = data;
		cached[id] = currentCache;
		saveCache(cached);
		return data;
	}
	LOG(`Load ${id}`);
	return data;
}

async function init() {
	try {
		const content = await readFile(cacheFile);
		LOG(`Loading cache from ${cacheFile}`);
		cached = deserialize(JSON.parse(content.toString()));
		await cleanCache();
	} catch (error) {
		LOG(`Error reading cache: ${error.message}`);
		LOG("Starting with a new cache");
	}
}

init();
