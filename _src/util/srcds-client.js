import {
	getChallenge,
	getMessageTypeID,
	getSinglePayload,
	isMulti,
	REQUEST_TYPE,
} from "./buffers.js";
import { newLogger } from "./logger.js";

const LOG = newLogger("UDP Client", "blue");
const LOGGERS = {};
/**
 * @type {[ip:string]:Request[]}
 */
const TF2_PENDING_REQUESTS = {};
let client;

/**
 *
 * @typedef {{message:Buffer,type:string[1],cb:(response:Response)=>void}} Request
 * @typedef {{request:Request,message:Buffer,type:string[1]}} Response
 */

function getLogger(ip) {
	if (typeof ip !== "string") {
		return;
	}
	LOGGERS[ip] = LOGGERS[ip] || newLogger(ip, "red").team("red").chat("UDP");
	return LOGGERS[ip];
}

function getClient() {
	if (Object.keys(TF2_PENDING_REQUESTS).length && !client) {
		client = dgram
			.createSocket("udp4")
			.on("message", receiveMessage)
			.on("close", onClose)
			.on("error", LOG.error);
	}
	return client;
}

function onClose() {
	LOG.logout();
	client = null;
}
/**
 *
 * @param {Buffer} msg
 * @param {dgram.RemoteInfo} param1
 */
function receiveMessage(msg, { address, port }) {
	const ip = `${address}:${port}`;
	getLogger(ip)(msg.toString("hex"));
	isMulti(msg)
		? (LOG("Unable to handle multi-part packets yet."), finishResponse(ip))
		: handleResponse(ip, getSinglePayload(msg));
}

/**
 *
 * @param {string} ip
 * @param {Buffer} message
 */
function handleResponse(ip, message) {
	const request = TF2_PENDING_REQUESTS[ip]?.[0];

	if (request) {
		const type = getMessageTypeID(message);

		if (type === REQUEST_TYPE.S2C_CHALLENGE) {
			LOG("Received challenge. Responding...");
			const challenge = getChallenge(message);
			request.resolved = true;
			const challengePrefix =
				request.type === REQUEST_TYPE.A2S_PLAYER
					? request.message.subarray(0, 5)
					: request.message;
			sendRequest(
				ip,
				Buffer.concat([challengePrefix, challenge]),
				request.cb,
			);
		} else {
			request.resolved = true;
			request.cb({
				request: {
					type: request.type,
					message: request.message,
				},
				type,
				message,
			});
		}
	} else {
		LOG(`Received unexpected response from ${ip}`);
	}

	finishResponse(ip);
}

/**
 *
 * @param {String} ip
 */
function finishResponse(ip) {
	const request = TF2_PENDING_REQUESTS[ip]?.shift();
	if (request && !request.resolved && typeof request.cb === "function") {
		request.cb({
			request: {
				type: request.type,
				message: request.message,
			},
			type: null,
			message: null,
		});
	}
	nextRequest(ip);
}

/**
 *
 * @param {string} ip
 * @param {Buffer} message
 * @param {undefined|(response:Response)=>void} cb
 * @returns
 */
export function sendRequest(ip, message, cb) {
	if (typeof ip !== "string" || !(message instanceof Buffer)) {
		throw new Error("Invalid ip or message", { ip, message });
	}

	const go = cb => {
		if (!cb) {
			throw new Error("Invalid response callback", { ip, message, cb });
		}
		const request = {
			message,
			cb,
			type: getMessageTypeID(message, 4),
			resolved: false,
		};
		if (TF2_PENDING_REQUESTS[ip] && TF2_PENDING_REQUESTS[ip].length) {
			TF2_PENDING_REQUESTS[ip].push(request);
		} else {
			TF2_PENDING_REQUESTS[ip] = [request];
			getLogger(ip);
			nextRequest(ip);
		}
	};

	if (cb) {
		go(cb);
	} else {
		return new Promise((resolve, reject) => {
			go(resolve);
		});
	}
}

/**
 *
 * @param {string} ip
 * @returns
 */
function nextRequest(ip) {
	if (typeof ip !== "string") {
		return;
	}
	const client = getClient();
	if (
		!Object.keys(TF2_PENDING_REQUESTS).length ||
		!TF2_PENDING_REQUESTS[ip]?.length
	) {
		LOGGERS[ip]?.logout("");
		delete TF2_PENDING_REQUESTS[ip];
		if (!Object.keys(TF2_PENDING_REQUESTS).length && client) {
			client.close();
		}
	} else {
		const msg = TF2_PENDING_REQUESTS[ip][0].message;
		const [address, port] = ip.split(":");
		LOG.chat("UDP")(msg.toString("hex")).chat(null);
		client.send(msg, 0, msg.length, parseInt(port) || port, address);
		setTimeout(() => finishResponse(ip), 10000);
	}
}
