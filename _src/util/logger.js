const format = (...n) => `\x1b[${n.join(";") || 0}m`;

const TEAMS = {
	RED: format(30 + 0b001),
	BLU: format(30 + 0b100),
	YELLOW: format(30 + 60 + 0b110),
};

const teams = { red: 0, blu: 0 };

const timestamp = () => {
	const date = new Date();
	return `${date.getFullYear()}${(date.getMonth() + 1).toString().padStart(2, "0")}${date.getDate().toString().padStart(2, "0")}${date.getHours().toString().padStart(2, "0")}${date.getMinutes().toString().padStart(2, "0")}${date.getSeconds().toString().padStart(2, "0")}${date.getMilliseconds().toString().padStart(3, "0")}`;
};

function setChat(logger, chat) {
	if (!logger) {
		return logger;
	}
	logger.c = chat;
	return logger;
}

function setTeam(logger, team) {
	if (!logger) {
		return logger;
	}
	if (logger.t && teams.hasOwnProperty(team)) {
		if (logger.t === team) {
			return logger;
		}
		console.log(
			`${logger.id} changed team to ${TEAMS[team.toUpperCase()]}`,
		);
	} else {
		team = teams.blu > teams.red ? "red" : "blu";
		if (logger.t === team) {
			return logger;
		}
		console.log(
			`${logger.id} was automatically assigned to team ${team.toUpperCase()}`,
		);
	}
	teams[team]++;
	logger.t = team;
	return logger;
}

function login(logger, team) {
	if (!logger || logger.loggedin) {
		return logger;
	}
	console.log(`${logger.id} has joined the game`);
	logger.loggedin = true;
	setTeam(logger, team);

	if (logger.timeout) {
		clearTimeout(logger.timeout);
	}
	logger.timeout = setTimeout(() => {
		logout(logger, "Inactive");
	}, 5000);

	return logger;
}

function logout(logger, reason = "Function invoked") {
	if (!logger || !logger.loggedin) {
		return logger;
	}
	console.log(`${logger.id} left the game (${reason})`);
	logger.loggedin = false;
	if (teams.hasOwnProperty(logger.t)) {
		teams[logger.t]--;
	}
	logger.t = null;
	if (logger.timeout) {
		clearTimeout(logger.timeout);
	}
	return logger;
}

function log(logger, type, ...a) {
	login(logger, logger.t);
	const teamFormat = TEAMS[logger.t.toUpperCase()];
	const name = teamFormat + logger.id + format();
	if (typeof a[a.length - 1] !== "function") {
		console[type || "info"](
			`[${timestamp()}] ${type ? `*${type}* ` : ""}${
				logger.c ? `(${logger.c.toUpperCase()}) ` : ""
			}${name}:`,
			...a,
		);
		return logger;
	}

	// timer
	const fn = a.pop();
	return new Promise(async res => {
		console.time(a.join(" "));
		await fn();
		console.timeLog(a.join(" "));
		res(logger);
	});
}

export function newLogger(id, team) {
	const info = (...a) => log(logger, null, ...a);

	const logger = Object.assign(info, {
		id,
		t: null,
		c: null,
		loggedin: false,
		logout: () => logout(logger),
		team: (team = team) => setTeam(logger, team),
		chat: chat => setChat(logger, chat),
		login: () => login(logger, logger.t),
		info,
		warn: (...a) => log(logger, "warn", ...a),
		error: (...a) => log(logger, "error", ...a),
	});

	if (team) {
		logger.team(team);
	}
	return logger;
}
