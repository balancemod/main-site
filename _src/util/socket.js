import { Server } from "socket.io";
import { getMostPopularServers, getTotalPlayerCount } from "./servers";
import { newLogger } from "./logger.js";
import { servers } from "./game-servers.js";

const LOG = newLogger("Socket Server");
LOG.chat("Socket");

function shortArrayString(arr) {
	const keys = new Set();
	const types = new Set();

	for (const item of arr) {
		const type =
			typeof item === "object"
				? item.constructor.name === "Object"
					? null
					: item.constructor.name
				: typeof item;
		type
			? types.add(type)
			: Object.keys(item).forEach(key => keys.add(key));
	}

	if (keys.size) {
		types.add(`{${Array.from(keys.values()).join(",")}}`);
	}
	const typeStr = Array.from(types.values()).join(",");

	return `[${arr.length} items of type ${types.size > 1 ? `{${typeStr}}` : typeStr}]`;
}

export function setupSocket(server, site) {
	let anyoneConnected = false;
	function send(socket, event, ...data) {
		LOG(
			event,
			"-",
			...data.map(v => {
				if (Array.isArray(v)) {
					return shortArrayString(v);
				}
				return v;
			}),
		);
		socket.emit(event, ...data);
	}

	function connection(socket) {
		LOG("New Client joined");
		let CLOG = newLogger(socket.id);
		CLOG.chat("Socket");
		CLOG.login();
		send(socket, "connection");

		send(io, "debug", "Hello From Server");

		if (process.env.NODE_ENV !== "production" && !anyoneConnected) {
			anyoneConnected = true;
			send(socket, "refresh");
		}

		socket.onAny((eventName, data) => {
			CLOG(eventName, "-", data);
		});

		socket.on("player-count", async () => {
			send(socket, "player-count", await getTotalPlayerCount(site));
		});

		socket.on("servers", async options => {
			send(socket, "servers", await getMostPopularServers(site));
		});

		socket.on("test", async query => {
			const room = servers(query);
			if (room) {
				socket.join(room);
			}
		});

		socket.on("disconnect", reason => {
			CLOG.logout(reason);
			CLOG = null;
		});
	}

	const io = new Server(server);

	io.on("connection", connection);
}
