/*
Order of events:
C socket.emit("servers",{query})
S break {query} down to keys, filter, sort
S if filter specifies an ip
	only that ip
  else
	all ips
S send udp message to the ips which each return a buffer
S extract the information from the buffer required to check with filter
S if filter not match skip to next ip
S if filter match, get requested keys and sort by requested sorting
S send resulting information to client
C socket.on("servers",yay i have the information i asked for)

start clinet does socket.emit("servers",{query})


end server does socket.emit("servers",[response])

resources
- function to parse query into keys, filter, sort
- udp servers that returns everything as a buffer
*/

import { cache } from "./cache.js";
import { sendRequest } from "./srcds-client.js";
import {
	REQUEST,
	REQUEST_TYPE,
	getShort,
	getByte,
	getString,
	getStringLen,
	getStringEnd,
} from "./buffers";

const SERVER_IPS = [
	"62.210.24.135:27015",
	"64.140.150.232:27015",
	"206.189.81.187:27015",
	"212.129.60.25:27015",
	"5.134.64.45:27025",
	"5.134.64.45:27035",
	"162.245.221.65:27055",
	"162.245.221.65:27025",
	"162.245.221.65:27015",
	"162.245.221.65:27045",
	"63.141.241.26:27025",
	"213.202.212.22:27015",
	"213.202.212.22:27020",
	"213.202.212.22:27030",
	"134.195.12.137:27015",
	"134.195.12.137:27025",
	"134.195.12.137:27035",
	"134.195.12.137:27055",
];
const QUERIES = {};
const QUERIES_BY_IP = {};
const SERVER_INFO = {};

const SERVER_INFO_SCHEMA = {
	header: 1,
	protocol: 1,
	name: 0,
	map: 0,
	folder: 0,
	game: 0,
	id: 2,
	players: 1,
	maxPlayers: 1,
};

function queryToID(options) {
	const encodeKey = {
		players: "p",
		ip: "i",
		map: "m",
		smallmode: "smd",
		mode: "md",
		gamemode: "gm",
		smallgamemode: "sgm",
		region: "r",
		flag: "f",
	};
	const keys = [
		"players",
		"ip",
		"map",
		"small",
		"mode",
		"game",
		"region",
		"flag",
	];
	const encode = obj => {
		//console.log(obj);
		if (obj) {
			if (typeof obj === "object") {
				if (!Array.isArray(obj)) {
					obj = Object.entries(obj);
				}
				obj = obj.map(encode).sort().join("");
				return obj;
			}
			if (typeof obj === "string") {
				for (const key of keys) {
					obj = obj.split(key).join(key[0]);
				}
			}
		}
		return obj || "";
	};
	return encode(options);
}

function queryToOptions(options) {
	let keys = [];
	let filter = {};
	let sortAttr = null;
	let sortDir = 0;
	if (typeof options === "string") {
		keys = [options];
	} else if (Array.isArray(options)) {
		keys = options;
	} else if (typeof options === "object" && options !== null) {
		keys = Object.keys(options);
		const entries = Object.entries(options);
		[sortAttr, sortDir] =
			entries.find(([_, v]) => typeof v === "boolean") || [];
		if (typeof sortDir === "boolean") {
			sortDir = sortDir ? 1 : -1;
		}
		filter = Object.fromEntries(
			entries.filter(([_, v]) => !!v && typeof v !== "boolean"),
		);
	}
	return { keys, filter, sortAttr, sortDir };
}

function parseQueryToIps(query) {
	const loadIp = (ip, query = "", keys = []) => {
		if (
			ip &&
			query &&
			keys &&
			typeof ip === "string" &&
			typeof query === "string" &&
			Array.isArray(keys)
		) {
			const ipQuery = (QUERIES_BY_IP[ip] ||= {
				queries: [],
				keys: [],
			});
			ipQuery.queries.push(query);
			ipQuery.keys.push(...keys);
		}
	};

	const { keys, filter } = QUERIES[query];
	if (
		filter?.ip &&
		typeof filter.ip === "string" &&
		SERVER_IPS.has(filter.ip)
	) {
		loadIp(filter.ip, query, keys);
	} else {
		SERVER_IPS.forEach(ip => loadIp(ip, query, keys));
	}
}

/**
 *
 * @param {Buffer} buffer
 * @param {{[key:string]:number}} schema
 * @param {keyof schema} key
 * @returns
 */
function getValue(buffer, schema, key) {
	const hasKey = Object.keys(schema).findIndex(k => k === key);
	let pos = 0;

	if (hasKey === -1) {
		throw new Error("Invalid key", { buffer, schema, key });
	}

	for (const [k, v] of Object.entries(schema)) {
		if (key === k) {
			return [getString, getByte, getShort][v](buffer, pos);
		}
		if (v) {
			pos += v;
		} else {
			pos += getStringLen(buffer, pos);
		}
	}
}

async function getInfo(ip) {
	const hexString = await cache(`${ip}-server-info`, async () => {
		const { type, message } = await sendRequest(ip, REQUEST.A2S_INFO);
		if (type === REQUEST_TYPE.S2C_INFO) {
			return message.toString("hex");
		}
	});
	if (typeof hexString === "string") {
		return Buffer.from(hexString, "hex");
	}
}

const keyDependents = {
	notfull: "maxPlayers",
	host: "name",
	gamemode: "map",
};
keyDependents.notempty = keyDependents.notfull;
keyDependents.region =
	keyDependents.regionId =
	keyDependents.domain =
	keyDependents.mode =
	keyDependents.shortmode =
		keyDependents.host;
keyDependents.shortgamemode = keyDependents.gamemode;

async function queryKeys(ip) {
	try {
		const buffer = await getInfo(ip);

		if (buffer) {
			const serverInfo = (SERVER_INFO[ip] ||= {});
			if (ip) {
				Object.keys(SERVER_INFO_SCHEMA).forEach(k => {
					const key = keyDependents[k] || k;
					if (key) {
						serverInfo[key] = getValue(buffer, SERVER_INFO_SCHEMA, key);
					}
				});
			}
			return serverInfo;
		}
	} catch (error) {
		console.error(`Error querying server ${ip}:`, error);
	}

	return null;
}

async function updateQueries() {
	await Promise.all(
		Object.entries(QUERIES_BY_IP).map(([ip, { keys }]) => {
			return queryKeys(ip, keys);
		}),
	);
	console.log(SERVER_INFO);

	for (let index = 0; index < array.length; index++) {
		const element = array[index];
	}
}

export function servers(query, cb) {
	const id = queryToID(query);
	const options = queryToOptions(query);

	if (!QUERIES.hasOwnProperty(id)) {
		QUERIES[id] = options;
		QUERIES[id].cb = cb;
		parseQueryToIps(id);
		updateQueries();
	}

	return id;
}
