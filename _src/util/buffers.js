// Constants
export const REQUEST_TYPE = {
	S2C_CHALLENGE: "\x41",
	S2C_PLAYER: "\x44",
	S2C_INFO: "\x49",
	A2S_INFO: "\x54",
	A2S_PLAYER: "\x55",
};

export const REQUEST_HEADER = Buffer.from([0xff, 0xff, 0xff, 0xff]);
export const PLAYLOAD = Buffer.from("Source Engine Query\0", "utf-8");
export const QUERY_CHALLENGE = Buffer.from([0xff, 0xff, 0xff, 0xff]);

export const REQUEST = {
	A2S_INFO: Buffer.concat([
		REQUEST_HEADER,
		Buffer.from(REQUEST_TYPE.A2S_INFO),
		PLAYLOAD,
	]),
	A2S_PLAYER: Buffer.concat([
		REQUEST_HEADER,
		Buffer.from(REQUEST_TYPE.A2S_PLAYER),
		QUERY_CHALLENGE,
	]),
};

// Utility functions
export function positionCheck(buffer, position, margin = 1) {
	return (
		typeof margin === "number" &&
		Number.isFinite(margin) &&
		typeof position === "number" &&
		Number.isFinite(position) &&
		(Buffer.isBuffer(buffer) ||
			(position >= 0 && position < buffer.length - margin))
	);
}

export function getByte(buffer, position = 0) {
	return positionCheck(buffer, position)
		? buffer.readUInt8(position)
		: undefined;
}

export function getShort(buffer, position = 0, length = 2) {
	return positionCheck(buffer, position, length)
		? buffer[`readInt${length * 8}LE`](position)
		: undefined;
}

export function getFloat(buffer, position = 0) {
	return getShort(buffer, position, 4);
}

export function getStringEnd(buffer, position = 0) {
	return positionCheck(buffer, position)
		? buffer.indexOf(0, position) || buffer.length - 1
		: undefined;
}

export function getStringLen(buffer, position = 0) {
	return positionCheck(buffer, position)
		? getStringEnd(buffer, position) - position + 1
		: undefined;
}

export function getString(buffer, position = 0) {
	return positionCheck(buffer, position)
		? buffer.toString("utf8", position, getStringEnd(buffer, position))
		: undefined;
}

export function isMulti(buffer) {
	const header = getLong(buffer);
	return header === -2 || header === -1 ? header === -2 : false;
}

export function getLong(buffer, position = 0) {
	return getShort(buffer, position, 4);
}

export function getSinglePayload(buffer) {
	return isMulti(buffer) ? undefined : buffer.subarray(4);
}

export function getMessageTypeID(buffer, offset) {
	const byte = getByte(buffer, offset);
	return byte ? String.fromCharCode(byte) : undefined;
}

export function getChallenge(buffer) {
	return getMessageTypeID(buffer) === REQUEST_TYPE.S2C_CHALLENGE
		? buffer.subarray(1)
		: undefined;
}

export function printBuffer(buffer) {
	console.log(
		"Offset(h)    | 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F    | ASCII",
	);
	console.log(
		"-----------------------------------------------------------------------",
	);

	for (let i = 0; i < buffer.length; i += 16) {
		const chunk = buffer.slice(i, i + 16);
		const hexValues = chunk
			.toString("hex")
			.match(/.{1,2}/g)
			.join(" ");
		const asciiValues = chunk
			.toString("ascii")
			.replace(/[^a-zA-Z0-9]/g, " ");

		console.log(
			`${i.toString(16).padStart(8, "0")}    | ${hexValues.padEnd(48, " ")}    | ${asciiValues}`,
		);
	}
}
