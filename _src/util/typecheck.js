/**
 * THING[TYPE](ARGS) => THINGER
 *
 * THINGER[CONDITION](ARGS) => THINGER
 *
 * THINGER(OBJ)
 */

const TYPE_CHECK = (() => {
	const TYPE_CHECKER = (type, fn, conditions = {}) => {
		const SETUP_TYPE_CHECKER =
			(last, fn) =>
			(...args) => {
				const TYPE_CHECKER = last => v =>
					!last || (last(v) && !fn) || fn(...args)(v);
				const currentChain = TYPE_CHECKER(last);
				for (const k in conditions) {
					currentChain[k] = SETUP_TYPE_CHECKER(
						currentChain,
						conditions[k],
					);
				}
				return currentChain;
			};
		return SETUP_TYPE_CHECKER(
			v => !type || typeof v === type.toLowerCase(),
			fn,
		);
	};

	return {
		NULL: TYPE_CHECKER("undefined", () => v => !v),
		NUMBER: TYPE_CHECKER("number", null, {
			INTEGER: () => v => Number.isInteger(v),
			FINITE: () => v => Number.isFinite(v),
			SAFE: () => v => Number.isSafeInteger(v),
			NAN: () => v => Number.isNaN(v),
			MIN: min => v => v >= min,
			MAX: max => v => v <= max,
		}),
		STRING: TYPE_CHECKER("string", null, {
			EMPTY: v => v === "",
			LENGTH: length => v => v.length === length,
			CONTAINS: substring => v => v.includes(substring),
		}),
		BOOLEAN: TYPE_CHECKER("boolean"),
		OBJECT: TYPE_CHECKER("object", schema => v => {
			if (!v || typeof v !== "object" || Array.isArray(v)) {
				return false;
			}
			for (const key in schema) {
				if (!(key in v) || !schema[key](v[key])) {
					return false;
				}
			}
			return true;
		}),
		ARRAY: TYPE_CHECKER(
			"array",
			type => v => Array.isArray(v) && (type ? v.every(type) : true),
			{
				LENGTH: length => v => v.length === length,
			},
		),
	};
})();
Array.from().every();

function hello(fn, subFns) {
	// ...
}

let test = hello(
	(...args) =>
		v => {
			return v.startsWith("f");
		},
	{
		world:
			(...args) =>
			v => {
				return v !== "fish";
			},
		face:
			(...args) =>
			v => {
				return v === "face";
			},
	},
);
test()("finger"); // true
test().world()("finger"); // false
test().world()("fish"); // false
test().face()("face"); // true
test().face().world()("face"); // true
test().face().world()("fish"); // false
