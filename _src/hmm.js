import Eleventy from "@11ty/eleventy";

(async () => {
	const elev = new Eleventy();
	await elev.watch();
})();
