import { Router } from "express";
import { filterServers } from "../util/servers.js";
import { newLogger } from "../util/logger.js";

const LOG = newLogger("play");

export default function init(site) {
	const router = new Router();

	router.get("/play", async (req, res) => {
		try {
			let params = req.query;
			if (
				!params ||
				typeof params !== "object" ||
				!Object.keys(params).length
			) {
				params = {
					sort: "players",
					sortDir: false,
				};
			}
			const servers = await filterServers(params);
			const server = servers[0];
			res.redirect(308, `steam://connect/${server.connect}`);
		} catch (error) {
			LOG.error(`Error fetching servers: ${error.message}`);
			res.redirect(`/?error=${error.message}`);
		}
	});

	router.get("/p/:nickname", async (req, res) => {
		try {
			const servers = await filterServers({
				nickname: req.params.nickname,
			});
			const server = servers[0];
			res.redirect(308, `steam://connect/${server.connect}`);
		} catch (error) {
			LOG.error(`Error fetching servers: ${error.message}`);
			res.redirect(`/?error=${error.message}`);
		}
	});

	return { router };
}
