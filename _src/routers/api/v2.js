import { Router } from "express";
import { newLogger } from "../../util/logger.js";
import {
	filterServers,
	getAllServerOptions,
	getMostPopularServers,
	getServersUpdate,
	getTotalPlayerCount,
} from "../../util/servers.js";

const LOG = newLogger("API");

export default function init() {
	const router = Router();

	router.get("/debug", (_req, res) => {
		res.json({
			version: 2,
			error: null,
			response: "Hello From Server",
		});
	});

	router.get("/player-count", async (_req, res) => {
		try {
			const playerCount = await getTotalPlayerCount();
			res.json({
				version: 2,
				error: null,
				response: playerCount,
			});
		} catch (error) {
			LOG.error(`Error fetching player count: ${error.message}`);
			res.status(500).json({
				version: 2,
				error: "Internal Server Error",
				response: null,
			});
		}
	});

	router.get("/servers", async (req, res) => {
		try {
			const servers = await filterServers(req.query);
			res.json({
				version: 2,
				error: null,
				query: req.query,
				response: servers,
			});
		} catch (error) {
			LOG.error(`Error fetching servers: ${error.message}`);
			res.status(500).json({
				version: 2,
				error: "Internal Server Error",
				response: null,
			});
		}
	});

	router.get("/popular-servers", async (_req, res) => {
		try {
			const servers = await getMostPopularServers();
			res.json({
				version: 2,
				error: null,
				response: servers,
			});
		} catch (error) {
			LOG.error(`Error fetching servers: ${error.message}`);
			res.status(500).json({
				version: 2,
				error: "Internal Server Error",
				response: null,
			});
		}
	});

	router.get("/server-options", async (_req, res) => {
		try {
			const options = await getAllServerOptions();
			res.json({
				version: 2,
				error: null,
				response: options,
			});
		} catch (error) {
			LOG.error(`Error fetching server options: ${error.message}`);
			res.status(500).json({
				version: 2,
				error: "Internal Server Error",
				response: null,
			});
		}
	});

	router.get("/servers-update", async (_req, res) => {
		try {
			const servers = await getServersUpdate();
			res.json({
				version: 2,
				error: null,
				response: servers,
			});
		} catch (error) {
			LOG.error(`Error fetching servers update: ${error.message}`);
			res.status(500).json({
				version: 2,
				error: "Internal Server Error",
				response: null,
			});
		}
	});

	router.get("/seasons/:season", (req, res) => {
		try {
			const seasonData = require(
				`../../../_data/bmod_season_${req.params.season}`,
			);
			res.json({
				version: 2,
				error: null,
				response: seasonData,
			});
		} catch (error) {
			LOG.error(`Error fetching season data: ${error.message}`);
			res.status(500).json({
				version: 2,
				error: "Internal Server Error",
				response: null,
			});
		}
	});

	router.use((_req, res) => {
		res.status(404).json({
			version: 2,
			error: "Invalid Request",
			response: null,
		});
	});

	return { router };
}
