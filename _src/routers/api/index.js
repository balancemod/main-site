import { Router } from "express";
import v2 from "./v2.js";

export default function init(site) {
	const router = Router();

	//router.use("/api/v1",require("./v1").init(site).router);
	router.use("/api/v2", v2(site).router);
	return { router };
}
