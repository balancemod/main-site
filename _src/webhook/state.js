import { readFile, writeFile } from "node:fs/promises";
import { dirname, join } from "node:path";

const ROOT = join(dirname(import.meta.filename), "..");
const file = "state.json";
const filePath = process.env.BMOD_STATE || join(ROOT, file);

let state = {};

async function init() {
	try {
		const content = await readFile(filePath);
		console.log(`[State Info] Loading state from ${filePath}`);
		state = JSON.parse(content.toString());
	} catch (error) {
		console.log("[State Info] Error loading state", error.message);
		console.log("[State Info] Using a blank state");
	}
}

export async function getState(wh) {
	return state[wh];
}

export async function modifyState(wh, action) {
	const state = await action(wh, state);
	await setState(wh, state);
}

export async function setState(wh, value) {
	state[wh] = value;
	setTimeout(
		async state => {
			await writeFile(filePath, JSON.stringify(state, null, 2), {
				encoding: "utf-8",
			});
		},
		0,
		state,
	);
}

init();
