import { spawn } from "node:child_process";
import { readFile } from "node:fs/promises";
import {
	AttachmentBuilder,
	Client,
	GatewayIntentBits,
	REST,
	Routes,
	SlashCommandBuilder,
} from "discord.js";
import { updateWebhook } from "./webhook.js";

const client = new Client({ intents: [GatewayIntentBits.Guilds] });
// this will be for registering the slash commands
const rest = new REST();
const BModGuildId = "429078866682970112";

async function setupWebhook() {
	let token = "";
	let posts = [];
	let config = [];
	console.log("setting up webhook bot");
	try {
		config = JSON.parse(
			await readFile(
				process.env.BMOD_WEBHOOKS || "./webhook-config.json",
				"utf8",
			),
		);
		token = await readFile(
			process.env.BMOD_DISCORD || "./token-discord",
			"utf8",
		);
		posts = JSON.parse(
			await readFile(process.env.BMOD_POSTS || "./result-posts", "utf8"),
		);
		posts = posts.map(p => {
			p.date = new Date(p.date);
			return p;
		});
	} catch (error) {
		console.error(error);
	}

	if (!token) {
		console.log("No discord token was specified, not running the webhook");
		return;
	}

	client.login(token);
	rest.setToken(token);

	client.on("ready", async () => {
		console.log(`Logged in as ${client.user.tag}!`);

		console.log("registering commands...");
		await rest.put(Routes.applicationCommands(client.user.id), {
			body: [],
		});
		await rest.put(
			Routes.applicationGuildCommands(client.user.id, BModGuildId),
			{
				body: [
					new SlashCommandBuilder()
						.setName("update")
						.setDescription("Update web server"),
				],
			},
		);

		updateWebhook(client, config, posts);
	});

	client.on("interactionCreate", async interaction => {
		console.log("recieved interaction", {
			id: interaction.id,
			type: interaction.type,
		});
		if (interaction.isChatInputCommand) {
			console.log(`interaction ${interaction.id} is a chat command`, {
				id: interaction.commandId,
				guild: interaction.guildId,
				name: interaction.commandName,
				type: interaction.commandType,
			});
			switch (interaction.commandName) {
				case "update": {
					if (process.env.NODE_ENV !== "production") {
						await interaction.reply({
							content:
								"avoiding running this in dev environment for obvious reasons",
							ephemeral: true,
						});
						return;
					}
					const ping = spawn(
						"/run/current-system/sw/bin/nixos-rebuild",
						"switch --flake gitlab:balancemod/main-site/nixos --refresh --show-trace -L".split(
							" ",
						),
					);
					let buffer = Buffer.from("", "utf-8");

					const attachment = new AttachmentBuilder().setName(
						"ping.txt",
					);

					await interaction.reply({
						content: "ok",
						ephemeral: true,
					});

					const handleData = async chunk => {
						buffer = Buffer.concat(
							[buffer, chunk],
							buffer.length + chunk.length,
						);
						attachment.setFile(buffer);
						await interaction.editReply({
							files: [attachment],
						});
					};

					ping.stdout.on("data", handleData);
					ping.stderr.on("data", handleData);

					ping.on("error", () => {
						interaction.followUp({
							content: "there was an error running the command",
							ephemeral: true,
						});
					});
					ping.on("disconnect", () => {
						interaction.followUp({
							content: "discconnected from the command",
							ephemeral: true,
						});
					});
					ping.on("close", () => {
						interaction.followUp({
							content: "command closed",
							ephemeral: true,
						});
					});
					ping.on("exit", () => {
						interaction.followUp({
							content: "command exited",
							ephemeral: true,
						});
					});
					ping.on("spawn", () => {
						interaction.followUp({
							content: "command spawned",
							ephemeral: true,
						});
					});

					break;
				}
			}
		}
	});
}

// module.exports = setupWebhook
setupWebhook();
