import { EmbedBuilder, WebhookClient } from "discord.js";
import { getState, setState } from "./state.js";

const feedWatchers = {};
const threads = {};
const webhooks = {};

async function getChannelFromID(client, id) {
	if (!client || !id) {
		console.log("Unable to find channel ", id);
		return;
	}
	return await client.channels.fetch(id);
}

async function getWebhookFromId(channel, id) {
	if (!channel || !id) {
		return;
	}
	//return await client.fetchWebhook(id);
	let webhooks = await channel.fetchWebhooks();
	return webhooks.get(id);
}

function getWebhookFromToken({ id, token }) {
	if (!id || !token) {
		return;
	}
	return new WebhookClient({ id, token });
}

async function getThreadFromID(channel, id) {
	if (!channel || !id) {
		return;
	}
	return await channel.threads.fetch(id);
}

async function getWebhook(channel, name, reason) {
	if (!channel) {
		console.log("Invalid channel when creating webhook");
		return;
	}
	if (!name) {
		console.log("Invalid webhook name when creating webhook");
		return;
	}
	const webhooks = await channel.fetchWebhooks();
	const webhook = webhooks.find(webhook => webhook.name == name);
	if (webhook) {
		return webhook;
	}
	return await channel.createWebhook({ name, reason });
}

async function getThread(channel, name, reason) {
	if (!channel || !name) {
		return;
	}
	const threads = channel.threads;
	const thread = threads.cache.find(thread => thread.name == name);
	if (thread) {
		return thread;
	}
	return threads.create({ name, reason });
}

function createDiscordWebhookEmbed({
	title,
	url,
	date,
	image,
	content,
	author,
}) {
	let embed = new EmbedBuilder()
		.setTitle(title)
		.setURL(url)
		.setImage(image)
		.setTimestamp(date)
		.setColor("#FDA200");

	if (content && content.length && content.length < 4000) {
		embed.setDescription(content);
	}
	if (author) {
		embed.setAuthor(typeof author == "string" ? { name: author } : author);
	}
	return embed;
}

function filterObjProp(obj, filter) {
	let go = true;
	for (const key in filter) {
		if (typeof filter[key] === "object" && !Array.isArray(filter[key])) {
			go &= key in obj && filterObjProp(obj[key], filter[key]);
		} else if (Array.isArray(obj[key])) {
			go &= obj[key].includes(filter[key]);
		} else {
			go &= filter[key] === obj[key];
		}
	}
	return go;
}

async function setupWebhookAndThread(client, webhookInfo) {
	const channel = await getChannelFromID(client, webhookInfo.channel);
	let webhook = webhooks[webhookInfo.name];
	let thread = threads[webhookInfo.thread];
	if (!webhook) {
		webhook = await getWebhook(
			channel,
			webhookInfo.name,
			"For sending website posts to discord (there should only be one of these per filter)",
		);
		console.log("[New Webhook]", webhookInfo.name, webhook.id);
		webhooks[webhookInfo.name] = webhook;
	}
	if (webhookInfo.thread && !thread) {
		thread = await getThread(
			channel,
			webhookInfo.thread,
			"Thread to contain the posts while testing",
		);
		console.log("[New Thread]", thread.name, thread.id);
		threads[webhookInfo.thread] = thread;
	}
}

export async function sendArticle(client, article, webhookInfo) {
	let content;
	if (article.summary && article.summary.length) {
		content = article.summary.replace(/^#+ (.*)\n/gm, "__**$1**__");
	}

	if (!(webhookInfo.name in webhooks || webhookInfo.thread in threads)) {
		await setupWebhookAndThread(client, webhookInfo);
	}
	const webhook = webhooks[webhookInfo.name];
	const thread = threads[webhookInfo.thread];
	const message = await webhook.send({
		threadId: thread ? thread.id : null,
		// avatarURL: client.user.avatarURL({ dynamic: true }),
		username: article.author.name,
		avatarURL: article.author.iconURL,
		embeds: [
			createDiscordWebhookEmbed({
				title: article.title,
				url: article.url,
				date: article.date,
				content,
				author: article.author,
			}),
		],
	});

	if (!webhookInfo.thread) {
		const thread = await message.startThread({
			name: article.title,
			reason: "Post Comments",
		});
		webhook.send({
			threadId: thread.id,
			//avatarURL: client.user.avatarURL({ dynamic: true }),
			username: article.author.name,
			avatarURL: article.author.iconURL,
			content: "Discuss the update here",
		});
	}
}

export async function updateWebhook(client, config, posts) {
	const sortedPosts = posts.sort(
		(a, b) => a.date.getTime() - b.date.getTime(),
	);
	for (const article of sortedPosts) {
		for (const webhook of config) {
			console.log("updating webhook", webhook);
			const state = await getState(webhook.name);
			if (
				article.url == state?.url ||
				new Date(state?.date).getTime() >= article.date.getTime()
			) {
				continue;
			}

			if (filterObjProp(article, webhook.filter)) {
				await setState(webhook.name, {
					url: article.url,
					date: article.date,
				});
				console.log("SENDING", article.title, article.date);
				await sendArticle(client, article, webhook);
			}
		}
	}
}
