{ pkgs ? import <nixpkgs> {config = {
  android_sdk.accept_license = true;
  allowUnfree = true;
  };
}} : with pkgs;

let
  unstable = import (fetchTarball https://nixos.org/channels/nixos-unstable/nixexprs.tar.xz);

  crossPkgs = unstable {
    crossSystem = { config = "x86_64-w64-mingw32"; };
    overlays = [
      (import (builtins.fetchTarball "https://github.com/oxalica/rust-overlay/archive/master.tar.gz"))
    ];
  };

  rust = crossPkgs.buildPackages.rust-bin.stable.latest.default.override {
    extensions = [ "rust-src" "rust-analysis" ];
    targets = [ "x86_64-pc-windows-gnu" ];
  };
in 
mkShell rec {
	packages = [nodePackages.npm nodePackages.nodejs pkg-config];
	buildInputs = [
    gtk3
    glibc
    nsis
    lld
    libllvm
    crossPkgs.windows.mingw_w64_pthreads
   crossPkgs.windows.pthreads
	];

  nativeBuildInputs = with crossPkgs; [
    rust
  ];
	PKG_CONFIG_PATH= with builtins; concatStringsSep ":" (map (package: "${package.dev}/lib/pkgconfig") [
		glib
		gtk3
		libsoup_3
		webkitgtk_4_1
    openssl
    gst_all_1.gst-plugins-good
    gst_all_1.gst-plugins-bad
    gst_all_1.gst-plugins-ugly
	]);
}
