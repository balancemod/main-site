let TF2_FOLDER = null;
const ASSETS_ZIP = `${location.origin}/robot_animation_fixes_v7.zip`;

function cd(cwd, folder) {
	return Tauri
		? `${cwd}/${folder}`
		: cwd.getDirectoryHandle(folder, { create: true });
}
const Tauri = window.__TAURI__;

async function getDB() {
	if ("indexedDB" in window) {
		return new Promise((res, rej) => {
			const request = indexedDB.open("TF2_FOLDER", 1);
			request.onupgradeneeded = e => {
				const db = e.target.result;
				if (!db.objectStoreNames.contains("TF2_FOLDER")) {
					db.createObjectStore("TF2_FOLDER");
				}
			};
			request.onsuccess = e => {
				console.debug("Successfully opened database");
				res(e.target.result);
			};
			request.onerror = e => {
				alert("Error opening database");
				console.error("Error opening database", e.target.error);
				rej(e.target.error);
			};
		});
	}
}

function basename(text) {
	const arr = text.split("/");
	return arr[arr.length - 1];
}

async function saveTF2Folder() {
	if (Tauri) {
		// localstorage because only text???
		localStorage.setItem("TF2_FOLDER", TF2_FOLDER);
	} else {
		// save toindexdb
		const db = await getDB();
		const transaction = db.transaction("TF2_FOLDER", "readwrite");
		const store = transaction.objectStore("TF2_FOLDER");
		await new Promise((res, rej) => {
			const request = store.put(TF2_FOLDER, "TF2_FOLDER");
			request.onsuccess = res;
			request.onerror = e => rej(e.target.error);
		});
	}
}

async function loadTF2Folder() {
	if (Tauri) {
		// localstorage because only text???
		localStorage.getItem("TF2_FOLDER");
	} else {
		// get from indexdb
		const db = await getDB();
		const transaction = db.transaction("TF2_FOLDER", "readonly");
		const store = transaction.objectStore("TF2_FOLDER");
		TF2_FOLDER = await new Promise((res, rej) => {
			const request = store.get("TF2_FOLDER");
			request.onsuccess = e => res(e.target.result || null);
			request.onerror = e => rej(e.target.error);
		});
	}
	console.log(TF2_FOLDER);
}

async function createWriteStream(basePath, filePath) {
	let writeStream;
	if (Tauri) {
		try {
			writeStream = await Tauri.fs.open(`${basePath}/${filePath}`, {
				read: true,
				write: true,
				truncate: true,
				create: true,
			});
		} catch (error) {
			console.log(error);
		}
	} else {
		const fileHandle = await basePath.getFileHandle(filePath, {
			create: true,
		});
		writeStream = await fileHandle.createWritable();
	}
	return writeStream;
}

async function extract(zipPath, to) {
	const response = await fetch(zipPath);
	if (!response.ok) {
		alert("failed to fetch zip");
	}
	const zipBlob = response.blob();

	const zip = await JSZip.loadAsync(zipBlob);

	if (Tauri) {
		try {
			await Tauri.fs.mkdir(to, { recursive: true });
		} catch (error) {
			console.log(error);
		}
	}
	const files = Object.entries(zip.files);

	const percentPerFile = 1 / files.length;

	for (const [relPath, file] of files) {
		if (file.dir || relPath.endsWith(".txt")) {
			continue;
		}
		const content = await file.async(Tauri ? "uint8array" : "blob");
		const writable = await createWriteStream(to, relPath);
		try {
			await writable.write(content);
		} catch (error) {
			alert(`Error writing file ${to}`);
			console.error(error);
		} finally {
			await writable.close();
		}
	}

	alert("extraction complete");
}

async function pickTF2Folder() {
	const response = Tauri
		? await Tauri.dialog.open({
				directory: true,
				multiple: false,
			})
		: await showDirectoryPicker({
				id: "tf2-folder",
				mode: "readwrite",
			});
	console.log(response);
	TF2_FOLDER = response;
	await saveTF2Folder();
	updateUI();
}

async function installAssets() {
	if (!TF2_FOLDER) {
		alert("Please open the tf2 folder");
		await pickTF2Folder();
	}
	const tf2 = await cd(TF2_FOLDER, "tf2");
	const custom = await cd(tf2, "custom");

	await extract(ASSETS_ZIP, custom);
}

function updateUI() {
	const updateButton = document.getElementById("tf2-extract");
	if (TF2_FOLDER) {
		updateButton.classList.remove("hide");
	} else {
		updateButton.classList.add("hide");
	}

	const foldernameElement = document.getElementById(
		"tf2-folder-picker-selected",
	);
	if (foldernameElement) {
		document.getElementById("tf2-folder-picker-selected").innerText =
			(Tauri ? basename(TF2_FOLDER) : TF2_FOLDER?.name) ||
			"No folder selected";
	}
}

document
	.getElementById("tf2-folder-picker")
	?.addEventListener("click", async () => {
		await pickTF2Folder();
	});
document.getElementById("tf2-extract")?.addEventListener("click", async () => {
	await installAssets();
});

async function showFolderPicker() {
	document.getElementById("ap-tf2-folder")?.classList.remove("hide");
	await loadTF2Folder();
	updateUI();
}

document.addEventListener("DOMContentLoaded", async () => {
	if (
		localStorage.getItem("showFolderPicker") ||
		(!navigator.userAgent.toLowerCase().includes("win") &&
			"showDirectoryPicker" in window) ||
		Tauri
	) {
		showFolderPicker();
	} else {
		// Event listener for Alt+A key press
		document.addEventListener("keydown", async event => {
			if (event.altKey && event.key === "a") {
				showFolderPicker();
				localStorage.setItem("showFolderPicker", "true");
			}
		});
	}
});
