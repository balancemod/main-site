import { createItemCard } from "./item-card";
import modes from "../../_data/modes.json" with { type: "json" };
import regions from "../../_data/regions.json" with { type: "json" };
import { createRow } from "./html";
const currentGamesDiv = document.getElementById("current-games");
const startGameDiv = document.getElementById("start-game");
const serverTablePrompt = document.getElementById("server-table-prompt");
const serversTable = document.getElementById("server-table");
const serversTableCount = document.getElementById("server-count");

const getRegion = r => regions[r || "unknown"];
const getMode = m => modes[m || "unknown"];

function removeLoading() {
	if(currentGamesDiv) {
	const loading = currentGamesDiv.querySelector("p");
	if (loading) {
		loading.remove()
	};
}
}

function setPlayerCount(players, servers) {
	if(currentGamesDiv) {
		const title = currentGamesDiv.querySelector("h2");
		if (servers === 0) {
			title.innerText = "";
			currentGamesDiv.classList.add("hide");
		} else {
			title.innerText = `${players} ${players == 1 ? "player is" : "players are"} playing in ${servers} ${servers == 1 ? "server" : "servers"}`;

			currentGamesDiv.classList.remove("hide");
		}
	}
}


function setPlayIP(ip) {
	const playButton = document.querySelector("#bmod-home-play-btn");
	if(ip) {
		playButton.href = `steam://connect/${ip}`;
		playButton.removeAttribute("disabled")
	} else {
		playButton.removeAttribute("href")
		playButton.setAttribute("disabled","true")
	}
	return playButton;
}



function organiseServers(servers, key) {
	if (!key) {
		return servers;
	}
		return servers.reduce((servers, server) => {
			const group = server[key];
			servers[group] = [...(servers[group] || []), server]
			return servers;
		}, {});
}

function createSection(name,{icon,code}, level=3) {
	const header = document.createElement(`h${level}`)
		header.innerText = name;

		if (icon) {
			const image = new Image()
			image.alt = code;
			image.src = icon;
			image.addEventListener("load", () => {
				header.prepend(image)
			})
		}
	return header
}



fetch("/api/v2/player-count")
	.then(stream => stream.json())
	.then(({ response, error }) => {
		removeLoading();
		if (error) {
			console.error(error);
		} else {
			setPlayerCount(response.players, response.servers);

			
		}
	})


fetch("/api/v2/servers?sort=players&sortDir=desc&empty=false").then(stream => stream.json()).then(({ response:servers, error }) => {
	if (error ) {
		console.error(error);
	} else if(currentGamesDiv) {
		const prevServers = currentGamesDiv.querySelectorAll(".server")
		prevServers.forEach(s => s.remove())

		setPlayIP(servers[0]?.ip)

		const serverLinks = servers.map(server => {

			const card = createItemCard({
				name: server.map,
				image: getRegion(server.region).icon,
				tags: server.tags,
				number: server.players,
				url: `steam://connect/${server.connect}`
			})
			card.classList.add("server")
			return card
		})

		currentGamesDiv.append(...serverLinks)
	}
})

fetch("/api/v2/servers?empty=true").then(stream => stream.json()).then(({ response, error }) => {
	if (error) {
		console.error(error);
	} else if(startGameDiv) {
		const prevServers = startGameDiv.querySelectorAll(".server, h3")
		prevServers.forEach(s => s.remove())
		startGameDiv.prepend(createSection("Start new game", {},2))

		const regionalServersDiv = startGameDiv.querySelector(".regions")
		if(regionalServersDiv) {
			const regional = organiseServers(response, "region");
			for (const regionName in regional) {
				const region = getRegion(regionName);
				const servers = regional[regionName];
				const serverLinks = servers.map(server => {
					const card = createItemCard({
						tags: server.map,
						image: getMode(server.mode).icon,
						name: server.mode,
						url: `steam://connect/${server.connect}`,
					});
					card.classList.add("server");
					return card;
				});

				regionalServersDiv.appendChild(
					createSection(regionName, region),
				);
				regionalServersDiv.append(...serverLinks);
			}
		}

		const modalServersDiv = startGameDiv.querySelector(".modes")
		if(modalServersDiv) {
			const modal = organiseServers(response, "mode");
			for (const modeName in modal) {
				const mode = getMode(modeName);
				const servers = modal[modeName];
				const serverLinks = servers.map(server => {
					const card = createItemCard({
						name: server.map,
						image: getRegion(server.region).icon,
						tags: server.region,
						url: `steam://connect/${server.connect}`,
					});
					card.classList.add("server");
					return card;
				});

				modalServersDiv.appendChild(createSection(modeName, mode));
				modalServersDiv.append(...serverLinks);
			}
		}
	}
})

function serverTableClick(e,tr, server) {
	serversTable.querySelectorAll(".active").forEach(s=>s.classList.remove("active"))
	tr.classList.add("active")

	if (e.detail === 2) {tableConnect()};
}

const getTableActive = () => serversTable.getElementsByClassName("active")[0];

function tableIp(e) {
	const server = getTableActive().server;
	if (server) {
		prompt(`${server.name}`,server.connect)
	} else {
		alert("Invalid Server")
	}
}

function tableCommand() {
	const server = getTableActive().server;
	if (server) {
		prompt(`${server.name}`,`connect ${server.connect}`)
	} else {
		alert("Invalid Server")
	}
}
function tableSteam() {
	const server = getTableActive().server;
	if (server) {
		prompt(`${server.name}`,`steam://connect/${server.connect}`)
	} else {
		alert("Invalid Server")
	}
}
function tablePlay() {
	const server = getTableActive().server;
	if (server) {
		prompt(`${server.name}`,`https://${location.host}/play/?ip=${server.connect}`)
	} else {
		alert("Invalid Server")
	}
}

function tableConnect() {

	const server = getTableActive().server;
	if (server) {
		location.href = `steam://connect/${server.connect}`
	} else {
		alert("Invalid Server")
	}
}



function refreshTable() {
	const title = document.getElementById("server-list-title");
	if (!title)  {return};
	title.innerText = "Servers - Refreshing";

	fetch("/api/v2/servers").then(stream => stream.json()).then(({ response, error }) => {
	title.innerText = "Servers"
		serversTable.querySelectorAll(".server").forEach(s => s.remove())
		serversTableCount.innerText = `Servers (${response.length})`
		
		const rows = response.map(server => {
			if(!server.map) {return}
			const row = document.createElement("tr")
			row.append(...createRow({
				region: "",
				mode: "",
				name: server.name||"",
				players: `${server.players||""}/${server.maxPlayers||""}`,
				map: server.map||"",
			}, {
				region: cell => {
					cell.innerHTML = `<img src="${getRegion(server.region).icon}" alt="${server.region}"/>`
					// cell.dataset.balloon = server.region;
				},
				mode: cell => {
					cell.innerHTML = `<img src="${getMode(server.mode).icon}" alt="${server.mode}" />`;
					cell.dataset.balloon = server.mode;
				}
			}))
			row.classList.add("server")

			row.addEventListener("click",(e)=> {
				serverTableClick(e,row,server)
			})
			row.server = server;
			return row
		}).filter(Boolean)

		serversTable.append(...rows)
	});
}

document.getElementById("server-list-open")?.addEventListener("click", () => {
	serverTablePrompt.style.display = "";
})
document.getElementById("server-list-close")?.addEventListener("click", () => {
	serverTablePrompt.style.display = "none";
})

serversTable?.querySelectorAll(".server").forEach(s => s.addEventListener("click",()=> {
	serverTableClick(row,server)
}))


document.getElementById("server-table-ip")?.addEventListener("click",tableIp)
document.getElementById("server-table-command")?.addEventListener("click",tableCommand)
document.getElementById("server-table-refresh")?.addEventListener("click",refreshTable)
document.getElementById("server-table-play")?.addEventListener("click",tablePlay)
document.getElementById("server-table-steam")?.addEventListener("click",tableSteam)
document.getElementById("server-table-connect")?.addEventListener("click",tableConnect)

refreshTable()