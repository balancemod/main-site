const socket = io();
let connectedbefore = false;

socket.on("connect", () => {
	console.log("Connected to the server");
});

socket.on("refresh", () => {
	if (connectedbefore) {
		location.reload();
	}
});

socket.on("disconnect", reason => {
	console.log("Disconnected from the server. Reason:", reason);
	connectedbefore = true;
});

socket.on("reconnect", attemptNumber => {
	console.log("Reconnected to the server on attempt", attemptNumber);
});

socket.on("reconnect_attempt", () => {
	console.log("Attempting to reconnect...");
});

socket.on("reconnect_error", error => {
	console.error("Reconnection error:", error.message);
});

socket.on("reconnect_failed", () => {
	console.error("All reconnection attempts failed. Please refresh the page.");
});

socket.on("debug", console.debug);
