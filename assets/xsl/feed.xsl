<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:atom="http://www.w3.org/2005/Atom">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
  <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
      <title>
        RSS Feed | <xsl:value-of select="/atom:feed/atom:title"/>
      </title>
      <link rel="stylesheet" href="/assets/styles/bmod.css"/>
	  <style>
	  body {
				background-image: url(/assets/images/footer.webp),url(/assets/images/pattern.webp);
			background-repeat: no-repeat,repeat-y;
			background-position: center bottom;
	  }
	  </style>
    </head>
	<body style="overflow-y:scroll">
	<div id="bmod-nav_watcher" data-intercept-watcher=""></div>
	<header id="bmod-nav" data-intercept-toggle="floating" data-intercept-root-margin="60px 0px 0px">
		<a href="/" id="bmod-logo">
			<span class="item-card  image">
				<img class="item-card_image " src="/assets/images/favicon.webp" > </img>
				<strong class="item-card_name hide-mobile">Balance Mod</strong>
			</span>
		</a>
	</header>
	<main >
		<div class="container">
			<div id="bmod-list">
			<xsl:for-each select="/atom:feed/atom:entry">
				<article id="bmod-content" class="bmod-panel">
					<div class="panel-header">
					<a>
						<xsl:attribute name="href">
							<xsl:value-of select="atom:link/@href"/>
						</xsl:attribute>
						<h2><xsl:value-of select="atom:title"/></h2>
					</a>
					<span class="page-meta">
					<a class="bmod-member" href="https://steamcommunity.com/id/heavyisgps">
					<!-- <span class="item-card tags  image">
						<img class="item-card_image " src="https://avatars.akamai.steamstatic.com/a332296c6b9146026d265b92c5a6b70fab30d8c9_full.jpg"></img>
						<strong class="item-card_name ">Heavy Is GPS</strong>
						<div class="item-card_tags ">
								PROJECT LEAD
						</div>
					</span> -->
					</a>
						<span class="item-card   ">
						<strong class="item-card_name "> <xsl:value-of select="substring(atom:updated, 0, 11)" /></strong>
					</span>
					</span>
					</div>
					<div class="panel-body margin">
						<xsl:value-of select="atom:summary"/>
					</div>
				</article>
				</xsl:for-each>c
			</div>
		</div>
	</main>
	</body>
	</html>
  </xsl:template>
</xsl:stylesheet>